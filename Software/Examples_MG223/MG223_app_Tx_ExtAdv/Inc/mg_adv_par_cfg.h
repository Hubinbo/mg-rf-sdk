/******************************************************************************
*  MG adv parameter config header file (auto generated 2021-05-19  17:13:35)  *
******************************************************************************/
#ifndef _MG_ADV_PAR_CFG_H_
#define _MG_ADV_PAR_CFG_H_

#define MG_TYPE_ADV_IND 0x00
#define MG_TYPE_ADV_DIRECT_IND 0x01
#define MG_TYPE_ADV_NONCONN_IND 0x02
#define MG_TYPE_ADV_EXT_IND 0x07
#define MG_TYPE_AUX_ADV_IND 0x09
#define MG_TYPE_AUX_SYNC_IND 0x0A
#define MG_TYPE_AUX_CHAIN_IND 0x0B
#define MG_TYPE_ADV_NOT_USED 0x0F

#define MG_ADV_PHY_1M 0x00
#define MG_ADV_PHY_500K 0x01
#define MG_ADV_PHY_125K 0x02
#define MG_ADV_PHY_2M 0x03

#define MG_ADV_CH_AUTO 0x80

u8 g_adv_type = MG_TYPE_ADV_EXT_IND;
u8 g_adv_ext1_type = MG_TYPE_ADV_EXT_IND;
u8 g_adv_ext2_type = MG_TYPE_AUX_ADV_IND;
u8 g_adv_ext3_type = MG_TYPE_AUX_CHAIN_IND;

u16 g_adv_interval = 150; //unit ms, for sync case, it is syn interval

//u8  gc_adv_data_n[] = {0};
//u8  gc_adv_data_n_len = 0;//0 means not used

u8 g_adv_data_pkt2[] = { //Extended(AUX_ADV_IND) AdvData
    2, 0x01, 0x04,
    17, 0x09, 'M', 'G', '2', '2', '3', '-', 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39};
u8 g_adv_data_pkt2_len = sizeof(g_adv_data_pkt2);

u8 g_adv_data_pkt3[] = { //ADV_SYNC_IND or AUX_CHAIN_IND AdvData
    23, 0xff, 0x9c, 0x05, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
u8 g_adv_data_pkt3_len = sizeof(g_adv_data_pkt3);

u8 g_ext_phy_sel = MG_ADV_PHY_1M;

u8 g_aux_ch_sel = MG_ADV_CH_AUTO;
u8 g_aux_phy_sel = MG_ADV_PHY_1M;
u8 g_interval_2_aux_step_num = 1;
u16 g_interval_2_aux_step[] = {
    18,
    135,
};

u8 g_interval_2_sync_step_num = 0; // 0 means not used
u16 g_sync_offset1_2_aux = 0;      // 0 means not used
u16 g_sync_offset2_2_sync = 0;     // 0 means not used
u8 g_aoa_enable_flag = 0;          // 0 means disabled or not used

u8 g_chain_ch_sel = MG_ADV_CH_AUTO;
u16 g_chain_offset = 33; //unit ms

#endif
