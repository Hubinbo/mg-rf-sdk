/*******************************************************************************
 * FilePath    : \Software\Examples_MG223\MG223_app_RfTest\Src\User\User_Printf.h
 * Version     : 1.0
 * Brief       : 
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2020-04-03 15:06:14
 * LastEditors : AndrewHu
 * LastEditTime: 2021-04-16 18:47:41
 * Description : 
 ******************************************************************************/
#ifndef __USER_PRINTF_H__
#define __USER_PRINTF_H__
/* Includes ------------------------------------------------------------------*/
#include <Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/*******************************************************************************
 * Function    : Printf_OK
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 17:19:07
 * Description : 
 ******************************************************************************/
void Printf_OK(void);
/*******************************************************************************
 * Function    : Printf_Error
 * Brief       : 发送错误及错误代码
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 17:19:07
 * Description : 
 ******************************************************************************/
void Printf_Error(u8 ErrorNum);
/*******************************************************************************
 * Function    : Printf_NewLine
 * Brief       : 
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-13 17:33:25
 * Description : 打印回车换行
 ******************************************************************************/
void Printf_NewLine(void);
/*******************************************************************************
 * Function    : Printf_ListSeparator_8byte
 * Brief       : 
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-14 11:30:47
 * Description : 分隔符
 ******************************************************************************/
void Printf_ListSeparator_8byte(void);
/*******************************************************************************
 * Function    : Printf_ListSpace_8byte
 * Brief       : 
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-14 11:30:47
 * Description : 空格
 ******************************************************************************/
void Printf_ListSpace_8byte(void);
/*******************************************************************************
 * Function    : Printf_PowerUp
 * Brief       : 
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-14 11:08:08
 * Description : 上电打印
 ******************************************************************************/
void Printf_PowerUp(void);

/* extern --------------------------------------------------------------------*/
#endif
