#ifndef _I2C_H_
#define _I2C_H_

extern void I2C_Write_Reg(uint8_t reg, uint8_t data);

extern uint8_t I2C_Read_Reg(uint8_t reg);

extern void I2C_Write_Buffer(uint8_t reg, uint8_t *dataBuf, uint8_t len);

extern void I2C_Read_Buffer(uint8_t reg, uint8_t *dataBuf, uint8_t len);

#endif
