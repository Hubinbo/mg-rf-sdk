/*******************************************************************************
 * FilePath    : /Software/Examples_MG223/MG223_app_Tx_ExtAdv/main.c
 * Version     : 1.0
 * Brief       : 
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2020-03-24 15:42:28
 * LastEditors : AndrewHu
 * LastEditTime: 2021-09-09 11:57:11
 * Description : 
 ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define StandbyMode 0x01 //停止工作，进入standby
#define SleepMode 0x02   //停止工作，进入sleep
#define NormalMode 0x03  //正常模式(RF Sleep)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
u8 runningMode = NormalMode; //
u8 UartRxData[50];
u8 UartRxDataLen;

u8 update_adv_data_flag = 0; //广播数据更新标志
/* Private function ----------------------------------------------------------*/
/* extern --------------------------------------------------------------------*/
extern u8 POWER_SAVE_OPT;
/*******************************************************************************
 * Function    : KeyTouch_Event
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-14 14:38:28
 * Description : 按键触发事件处理
 ******************************************************************************/
void KeyTouch_Event(void)
{
    // runningMode++;
    // if (runningMode > NormalMode)
    //     runningMode = StandbyMode;
    // Set_RuningMOde(runningMode);

    g_adv_data_pkt3[g_adv_data_pkt3_len - 1]++;
    update_adv_data_flag = 0x04; //pkt3 data changed
}
/*******************************************************************************
 * Function    : main
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2020-03-24 16:11:19
 * Description : 启动函数，主循环
 ******************************************************************************/
int main(void)
{
    u8 irq_status;
    u32 time;

    Init_System();

    Printf_PowerUp(); //上电信息打印

    if (POWER_SAVE_OPT == POWER_SAVE_STANDBY)
        BLE_RESET();

    BLE_Init(BLE_TX_POWER, TRUE); //蓝牙初始化
    Uart_Send_String("BLE init ok.\r\n", 14);

    BLE_TX_Ext();

    time = GetSysTickCount();
    while (1)
    {
        if ((GetSysTickCount() - time) >= 5)
        { //5ms
            time = GetSysTickCount();
            Key_Scan();
        }

        //串口示例，收到数据直接打印
        Uart_Recived(UartRxData, &UartRxDataLen);
        if (UartRxDataLen != 0)
        {
            Uart_Send_String(UartRxData, UartRxDataLen);
            UartRxDataLen = 0;
        }

        //
        LED_GREEN_OFF();
        irq_status = radio_getIrqState();
        if (0 == irq_status)
        {
            Delay_ms(20);
        }
        if (irq_status & (0x01 | 0x04 | 0x10))
        {
            LED_GREEN_ON(); //debug
        }
        if (irq_status & 0x04) //pkt2
        {
            if (g_pkt_types & AUX_ADV_IND_BIT)
                update_adv_ext_ind_par();
            if (g_pkt_types & AUX_CHAIN_IND_BIT)
                update_aux_chain_ind_par();

            //update pkt2 if changed
            if (update_adv_data_flag & 0x01)
            {
                update_adv_ext_ind_data(g_adv_data_pkt2, g_adv_data_pkt2_len);
                update_adv_data_flag &= 0xfe;
            }
        }
        if (irq_status & 0x01) //pkt3
        {                      //update pkt3 if changed
            if (update_adv_data_flag & 0x04)
            {
                update_aux_chain_ind_data(g_adv_data_pkt3, g_adv_data_pkt3_len);
                update_adv_data_flag &= 0xfb;
            }
        }
    }
}
