#ifndef _HARDWARECFG_H_
#define _HARDWARECFG_H_

#define HARD3LINESPI
#define COMM_SPI 0   // 3 line spi
#define COMM_I2C 1       //I2C OD with external PU resistor
#define COMM_I2C_SIMU_NOPU 2  //I2C simulation, PP, with NO resistor

#define COMM_OPT_SEL COMM_SPI

#if (COMM_OPT_SEL == COMM_SPI)
    #define MG_Read_Reg(a)     SPI_Read_Reg(a)
    #define MG_Write_Reg(a,b)  SPI_Write_Reg(a|0x40,b)
    #define MG_Read_Buffer(a,b,c)  SPI_Read_Buffer(a,b,c)
    #define MG_Write_Buffer(a,b,c) SPI_Write_Buffer(a,b,c)
#elif ((COMM_OPT_SEL == COMM_I2C)||(COMM_OPT_SEL == COMM_I2C_SIMU_NOPU))
    #define MG_Read_Reg(a)     I2C_Read_Reg(a)
    #define MG_Write_Reg(a,b)  I2C_Write_Reg(a,b)
    #define MG_Read_Buffer(a,b,c)  I2C_Read_Buffer(a,b,c)
    #define MG_Write_Buffer(a,b,c) I2C_Write_Buffer(a,b,c)
#else
    #error "COMMUNICATION INTERFACE SHOULD BE DEFINED!"
#endif

/*****************************************************
KEY: GPIOC
*****************************************************/
#define KEY_PORT       		GPIOC
#define KEY             	GPIO_Pin_1
#define KEY_GET()       	GPIO_ReadInputDataBit(KEY_PORT,KEY)

/*****************************************************
LED:
LED1		:PA2  	RED
LED2		:PA3	GREEN
*****************************************************/

#define LED1_PORT    		GPIOA
#define LED2_PORT    		GPIOA

#define LED1             	GPIO_Pin_2
#define LED2             	GPIO_Pin_3

#define	LED_RED_ON()		GPIO_SetBits(LED1_PORT,LED1)
#define	LED_RED_OFF()		GPIO_ResetBits(LED1_PORT,LED1)

#define	LED_GREEN_ON()		GPIO_SetBits(LED2_PORT,LED2)
#define	LED_GREEN_OFF()		GPIO_ResetBits(LED2_PORT,LED2)


/*****************************************************
BLE SPI:
BLE_CSN         : GPIOB
BLE_SCK         : GPIOB
BLE_MOSI_MISO   : GPIOB

BLE I2C:
BLE_SCL         : GPIOB
BLE_SDA         : GPIOB

BLE_IRQ         : GPIOC
*****************************************************/
#define BLE_CSN_PORT    	GPIOB
#define BLE_SCK_PORT    	GPIOB
#define BLE_MOSI_MISO_PORT  GPIOB

#define BLE_I2C_PORT    	GPIOB
#define BLE_SCL_PORT    	GPIOB
#define BLE_SDA_PORT        GPIOB

#define BLE_IRQ_PORT    	GPIOC
#define BLE_nRST_PORT       GPIOC

#define BLE_CSN			    GPIO_Pin_4
#define BLE_SCK      		GPIO_Pin_5
#define BLE_MOSI_MISO   	GPIO_Pin_6

//#define BLE_I2C           GPIO_Pin_4
#define BLE_SCL             GPIO_Pin_5
#define BLE_SDA             GPIO_Pin_6

#define BLE_IRQ             GPIO_Pin_0
#define BLE_nRST            GPIO_Pin_0

#ifdef HARD3LINESPI
    #define BLE_CSN_CLR()     	{SPI_BiDirectionalLineConfig(SPI_Direction_Tx);SPI_Cmd(ENABLE);GPIO_ResetBits(BLE_CSN_PORT,BLE_CSN);}
#else
    #define BLE_CSN_CLR()     	GPIO_ResetBits(BLE_CSN_PORT,BLE_CSN)
#endif

#define BLE_CSN_SET()    	GPIO_SetBits(BLE_CSN_PORT,BLE_CSN)

#define BLE_SCK_CLR()     	GPIO_ResetBits(BLE_SCK_PORT,BLE_SCK)
#define BLE_SCK_SET()    	GPIO_SetBits(BLE_SCK_PORT,BLE_SCK)

#define BLE_MOSI_MISO_CLR()	GPIO_ResetBits(BLE_MOSI_MISO_PORT,BLE_MOSI_MISO)
#define BLE_MOSI_MISO_SET()	GPIO_SetBits(BLE_MOSI_MISO_PORT,BLE_MOSI_MISO)

#define BLE_MOSI_MISO_GET()	GPIO_ReadInputDataBit(BLE_MOSI_MISO_PORT,BLE_MOSI_MISO)



//#define I2C_ENABLE()      GPIO_SetBits(BLE_I2C_PORT,BLE_I2C)
#if (COMM_OPT_SEL == COMM_I2C)
    #define GPIO_Mode_Out_Sel GPIO_Mode_Out_OD_HiZ_Slow
#else
    #define GPIO_Mode_Out_Sel GPIO_Mode_Out_PP_High_Slow
#endif

#define SCL_SET()           GPIO_SetBits(BLE_SCL_PORT,BLE_SCL)
#define SCL_CLR()           GPIO_ResetBits(BLE_SCL_PORT,BLE_SCL)

#define SDA_SET()           GPIO_SetBits(BLE_SDA_PORT,BLE_SDA)
#define SDA_CLR()           GPIO_ResetBits(BLE_SDA_PORT,BLE_SDA)

#define SDA_OUT()           GPIO_Init(BLE_SDA_PORT,BLE_SDA,GPIO_Mode_Out_Sel)
#define SDA_IN()            GPIO_Init(BLE_SDA_PORT,BLE_SDA,GPIO_Mode_In_PU_No_IT)
#define SDA_READ()          GPIO_ReadInputDataBit(BLE_SDA_PORT,BLE_SDA)

//#define BLE_IRQ_GET()       GPIO_ReadInputDataBit(BLE_IRQ_PORT,BLE_IRQ)

/*****************************************************
USART:
RX		:PC2
TX		:PC3
*****************************************************/

#define RX_PORT    			GPIOC
#define TX_PORT    			GPIOC

#define RX             		GPIO_Pin_2
#define TX             		GPIO_Pin_3


#endif
