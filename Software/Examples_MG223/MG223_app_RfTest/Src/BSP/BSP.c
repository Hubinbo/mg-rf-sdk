/*******************************************************************************
 * FilePath    : /Software/Examples_MG223/MG223_app_RfTest1/Src/BSP/BSP.c
 * Version     : 1.0
 * Brief       :
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2021-04-13 17:25:18
 * LastEditors : AndrewHu
 * LastEditTime: 2022-06-09 09:47:21
 * Description :
 ******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
// mcu fre:16MHz
//  (1/((1/(16000000/1000))*64))  Downcounter.TIM2  time:1ms
#define TIM2_1MS_VALUE 0x00FD

// mcu fre:16MHz
#define FREOSC 16000000UL
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/* extern --------------------------------------------------------------------*/

/*******************************************************************************
 * Function    : Delay_us
 * Brief       :
 * Parameter   : delayCnt
 * Returns     :
 * Date        : 2021-04-13 17:29:17
 * Description : Delay_us(10). delay time: 10us
 ******************************************************************************/
void Delay_us(uint16_t delayCnt)
{
	uint16_t temp0 = 0;
	for (temp0 = 0; temp0 < delayCnt; temp0++)
	{
		__asm("nop");
		__asm("nop");
		__asm("nop");
		__asm("nop");
		__asm("nop");
		__asm("nop");
	}
}
/*******************************************************************************
 * Function    : Delay_ms
 * Brief       :
 * Parameter   : delayCnt
 * Returns     :
 * Date        : 2021-04-13 17:29:40
 * Description :
 ******************************************************************************/
void Delay_ms(uint16_t delayCnt)
{
	uint16_t temp0 = 0;
	for (temp0 = 0; temp0 < delayCnt; temp0++)
	{
		Delay_us(1132);
	}
}
/*******************************************************************************
 * Function    : Init_Gpio
 * Brief       :
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-13 17:31:29
 * Description :
 ******************************************************************************/
void Init_Gpio(void)
{
	// reset all gpio
	GPIO_DeInit(GPIOA);
	GPIO_DeInit(GPIOB);
	GPIO_DeInit(GPIOC);
	GPIO_DeInit(GPIOD);

	// key set: input mode . no interrupt
	GPIO_Init(KEY_PORT, KEY, GPIO_Mode_In_FL_No_IT);

	// LED set: ouput mode.
	GPIO_Init(LED1_PORT, LED1, GPIO_Mode_Out_PP_Low_Slow);
	GPIO_Init(LED2_PORT, LED2, GPIO_Mode_Out_PP_Low_Slow);

	// GPIO_Init(BLE_IRQ_PORT,BLE_IRQ,GPIO_Mode_In_PU_No_IT);
	GPIO_Init(BLE_nRST_PORT, BLE_nRST, GPIO_Mode_Out_PP_High_Slow);

	GPIO_Init(GPIOB, GPIO_Pin_0, GPIO_Mode_In_PU_No_IT); // PB0 input pull-up
	GPIO_Init(GPIOD, GPIO_Pin_0, GPIO_Mode_In_PU_No_IT); // PB0 input pull-up

	GPIO_Init(RX_PORT, RX, GPIO_Mode_In_PU_No_IT);
	GPIO_Init(TX_PORT, TX, GPIO_Mode_Out_PP_High_Slow);

	// set LED off status
	GPIO_ResetBits(LED1_PORT, LED1);
	GPIO_ResetBits(LED2_PORT, LED2);

#if (COMM_OPT_SEL == COMM_SPI)
	// BLE soft_spi. CSN\ SCK\ MOSI_MISO\ :output mode . IRQ :input mode.no interrupt
	GPIO_Init(BLE_CSN_PORT, BLE_CSN, GPIO_Mode_Out_PP_High_Slow);
	GPIO_Init(BLE_SCK_PORT, BLE_SCK, GPIO_Mode_Out_PP_High_Slow);
	GPIO_Init(BLE_MOSI_MISO_PORT, BLE_MOSI_MISO, GPIO_Mode_Out_PP_High_Slow);

	// spi :csn : high  close spi .     sck:low
	BLE_CSN_SET();
	BLE_SCK_CLR();
#else
	/// GPIO_Init(BLE_I2C_PORT,BLE_I2C,GPIO_Mode_Out_PP_High_Slow);
	GPIO_Init(BLE_SCL_PORT, BLE_SCL, GPIO_Mode_Out_Sel);
	GPIO_Init(BLE_SDA_PORT, BLE_SDA, GPIO_Mode_Out_Sel);

	// I2C_ENABLE();
	SCL_SET();
	SDA_SET();
#endif
}
/*******************************************************************************
 * Function    : Init_Clock
 * Brief       :
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-13 17:32:00
 * Description :
 ******************************************************************************/
void Init_Clock(void)
{
	// reset system clock
	CLK_DeInit();
	// diable clock output
	CLK_CCOCmd(DISABLE);
	// HSI Prescaler 1.
	CLK_MasterPrescalerConfig(CLK_MasterPrescaler_HSIDiv1);

	// TIM2&USART clock enable .
	CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
	CLK_PeripheralClockConfig(CLK_Peripheral_USART, ENABLE);
}
/*******************************************************************************
 * Function    : Init_Timer
 * Brief       :
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-13 17:32:16
 * Description : TIM2
 ******************************************************************************/
void Init_Timer(void)
{
	// TIM2 reset
	TIM2_DeInit();
	// tim2 prescaler_64.CounterMode Down ,
	TIM2_TimeBaseInit(TIM2_Prescaler_64, TIM2_CounterMode_Down, TIM2_1MS_VALUE);
	// enable tim2 flag .enable interrupt flag
	TIM2_ITConfig(TIM2_IT_Update, ENABLE);

	// disable TIM2
	// TIM2_Cmd(DISABLE);
	TIM2_Cmd(ENABLE);
}
/*******************************************************************************
 * Function    : Init_SPI
 * Brief       :
 * Parameter   :
 * Returns     :
 * Date        : 2021-04-16 20:55:36
 * Description :
 ******************************************************************************/
void Init_SPI(void)
{
	SPI_DeInit();

	SPI_Init(SPI_FirstBit_MSB, SPI_BaudRatePrescaler_4, SPI_Mode_Master, SPI_CPOL_Low, SPI_CPHA_1Edge, SPI_Direction_1Line_Tx, SPI_NSS_Soft);
	SPI_Cmd(ENABLE);
}
/*******************************************************************************
 * Function    : Init_System
 * Brief       :
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-13 17:26:20
 * Description :
 ******************************************************************************/
void Init_System(void)
{
	// disable all interrupt
	disableInterrupts();
	Init_Clock();
	Init_Gpio();
	Init_Timer();
	Init_Uart();

#if (COMM_OPT_SEL == COMM_SPI)

#ifdef HARD3LINESPI
	Init_SPI();
#endif

#endif
	// enable all interrupt
	enableInterrupts();
}