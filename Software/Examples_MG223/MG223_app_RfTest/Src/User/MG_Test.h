/*******************************************************************************
 * FilePath    : \Software\Examples_MG223\MG223_app_RfTest\Src\User\MG_Test.h
 * Version     : 1.0
 * Brief       : 
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2021-04-16 10:31:34
 * LastEditors : AndrewHu
 * LastEditTime: 2021-04-16 19:38:44
 * Description : 
 ******************************************************************************/
#ifndef _MG_TEST_H_
#define _MG_TEST_H_
/* Includes ------------------------------------------------------------------*/
#include <Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define ErrorCode_CmdLen 0x01     //指令长度错误
#define ErrorCode_UnknownCmd 0x02 //指令错误，未知指令
#define ErrorCode_DeviceType 0x03 //错误设备
#define ErrorCode_Function 0x04   //功能错误
#define ErrorCode_TxPower 0x05    //发射功率
#define ErrorCode_Channel 0x06    //通道设置为空
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/*******************************************************************************
 * Function    : Fun_Test_Main
 * Brief       : 芯片测试
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 10:39:04
 * Description : 
 ******************************************************************************/
void Fun_Test_Main(void);
/* extern --------------------------------------------------------------------*/

#endif
