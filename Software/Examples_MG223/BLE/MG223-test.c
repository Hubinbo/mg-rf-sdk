/*******************************************************************************
 * FilePath    : /Software/Examples_MG223/BLE/MG223-test.c
 * Version     : 1.0
 * Brief       :
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2021-04-13 17:17:26
 * LastEditors : AndrewHu
 * LastEditTime: 2023-04-21 14:42:05
 * Description :
 ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/* extern --------------------------------------------------------------------*/

/*************************************************
Functions used for SRRC/FCC test
Called after BLE_Init()
Add while(1){} after calling
*************************************************/

// FCC/SRRC Carrier test
/*******************************************************************************
 * Function    : SRCC_CarrierTest
 * Brief       : 载波测试
 * Parameter   : {unsigned char} chan(0-39)
 * Returns     :
 * Date        : 2021-04-16 19:30:12
 * Description :
 ******************************************************************************/
void SRCC_CarrierTest(unsigned char chan) // N=(F-2402)/2
{
  u8 tmp[3] = {0x08, 0xc0, 0x80 | chan};

  MG_Write_Buffer(CONT_TEST_MODE, tmp, 3);

  MG_Write_Reg(XO_PD_EN, 0x10); // cont_wave=1,carrier_wave_en=0
  Delay_ms(5);

  MG_Write_Reg(WAKEUP_NOW, 1);
}

u8 AA_BQB_DTM[4] = {0x29, 0x41, 0x76, 0x71};
/*******************************************************************************
 * Function    : SRRC_PRBS9Test
 * Brief       : 调制波形
 * Parameter   : {unsigned char} chan
 * Parameter   : {unsigned char} pkt_pld
 * Parameter   : {unsigned char} pkt_len
 * Returns     :
 * Date        : 2021-04-16 19:29:14
 * Description :
 ******************************************************************************/
void SRRC_PRBS9Test_sdk(unsigned char chan, unsigned char pkt_pld, unsigned char pkt_len) // N=(F-2402)/2
{
  u8 tmp[5] = {0x48, 0xc0, 0x80 | chan, 0x00, 0x80 | pkt_pld};

  MG_Write_Buffer(CONT_TEST_MODE, tmp, 5);

  tmp[0] = 0x83; // scramble=0
  tmp[1] = AA_BQB_DTM[0];
  tmp[2] = AA_BQB_DTM[1];
  tmp[3] = AA_BQB_DTM[2];
  tmp[4] = AA_BQB_DTM[3];
  MG_Write_Buffer(CONFIG, tmp, 5);

  // crc init=0x555555

  MG_Write_Reg(XO_PD_EN, 0x00); // cont_wave=0,carrier_wave_en=0
  Delay_ms(5);

  // 625us: if len=37~115,  625*2: if len=116~255
  tmp[0] = 0x71; // 0x00271=625
  tmp[1] = 2;
  tmp[2] = 0;
  tmp[3] = 1; // RC_LOAD;
  MG_Write_Buffer(ADV_INTERV_RC_CFG, tmp, 4);

  tmp[0] = 0x08;
  tmp[1] = 0;
  tmp[2] = 0;
  tmp[3] = pkt_pld;
  tmp[4] = pkt_len; // length
  MG_Write_Buffer(ADV_PKT_CFG, tmp, 5);

  MG_Write_Reg(BASEBAND_CFG, 1); // SLP_XO=4, STOP=2, WKUP_EN=1

  MG_Write_Reg(WAKEUP_NOW, 1);
}

void Dealy(unsigned short delay)
{
  while (delay--)
  {
    __asm("nop");
  }
}

void SRRC_PRBS9Test(unsigned char chan, unsigned char pkt_pld, unsigned char pkt_len) // N=(F-2402)/2
{
  u8 tmp[5] = {0x48, 0xc0, 0x80 | chan, 0x00, 0x80 | pkt_pld};
  u16 delay = pkt_len << 4;
  delay += 0x110;

  MG_Write_Buffer(CONT_TEST_MODE, tmp, 5);

  tmp[0] = 0x83; // scramble=0
  tmp[1] = AA_BQB_DTM[0];
  tmp[2] = AA_BQB_DTM[1];
  tmp[3] = AA_BQB_DTM[2];
  tmp[4] = AA_BQB_DTM[3];
  MG_Write_Buffer(CONFIG, tmp, 5);

  // crc init=0x555555

  MG_Write_Reg(XO_PD_EN, 0x00); // cont_wave=0,carrier_wave_en=0
  Delay_ms(5);

  // 625us: if len=37~115,  625*2: if len=116~255
  tmp[0] = 0x71; // 0x00271=625
  tmp[1] = 2;
  tmp[2] = 0x10;
  tmp[3] = 1; // RC_LOAD;
  MG_Write_Buffer(ADV_INTERV_RC_CFG, tmp, 4);

  tmp[0] = 0x08;
  tmp[1] = 0;
  tmp[2] = 0;
  tmp[3] = pkt_pld;
  tmp[4] = pkt_len; // length
  MG_Write_Buffer(ADV_PKT_CFG, tmp, 5);

  TIM2_Cmd(DISABLE);
  while (1)
  {
    MG_Write_Reg(BASEBAND_CFG, 2);

    // add exit port
    //  if (KEY_GET() == RESET)
    //    break;
    if (GetRxTimeout() == FALSE)
      break; // received data,break

    tmp[0] = 0x08;
    tmp[1] = 0;
    MG_Write_Buffer(PA_CFG, tmp, 2);
    MG_Write_Reg(WAKEUP_NOW, 1);

    Dealy(0x0178); // adjustment range

    tmp[0] = 0x08;
    tmp[1] = 18;
    MG_Write_Buffer(PA_CFG, tmp, 2);

    Dealy(delay);
  }
  TIM2_Cmd(ENABLE);
}
/*******************************************************************************
 * Function    :
 * Brief       :
 * param        {*}
 * return       {*}
 * Description :
 * Date        : 2021-07-22 17:55:42
 ******************************************************************************/
static const unsigned char PayLoad[] = {
    0x00, 0xa4, 0xff, 0x83, 0xdf, 0x17, 0x32, 0x09, 0x4E, 0xD1, 0xE7, 0xCD, 0x8A, 0x91, 0xC6, 0xD5,
    0xC4, 0xC4, 0x40, 0x21, 0x18, 0x4E, 0x55, 0x86, 0xF4, 0xDC, 0x8A, 0x15, 0xA7, 0xEC, 0x92, 0xDF,
    0x93, 0x53, 0x30, 0x18, 0xCA, 0x34, 0xBF, 0xe2, 0x21, 0xe8};
/*******************************************************************************
 * Function    : RF_Test
 * Brief       :
 * param        {unsigned char} chan
 * param        {unsigned char} pkt_pld
 * param        {unsigned char} pkt_len
 * return       {*}
 * Description :
 * Date        : 2021-07-22 18:02:18
 ******************************************************************************/
void RF_Test(unsigned char chan, unsigned char pkt_pld, unsigned char pkt_len)
{
  u8 i, tmp[5] = {0x48, 0xc0, 0x80 | chan, 0x00, 0x80 | pkt_pld};
  u16 delay = pkt_len << 4;
  delay += 0x110;

  tmp[0] = 0x08;
  tmp[1] = 0;
  MG_Write_Buffer(PA_CFG, tmp, 2);

  Delay_ms(50);
  MG_Write_Reg(XO_PD_EN, 0x00);
  Delay_ms(50);

  // if (chan == 0)
  // {
  //   tmp[0] = 0xc2;
  //   tmp[1] = 0x98;
  //   tmp[2] = 0x03;
  //   tmp[3] = 0x55;
  //   MG_Write_Buffer(0x37, tmp, 4);
  // }

  MG_Read_Buffer(CONT_TEST_MODE, tmp, 3);
  tmp[2] &= 0xC0;
  tmp[2] |= chan;
  MG_Write_Buffer(CONT_TEST_MODE, tmp, 5);

  MG_Read_Buffer(0x18, tmp, 1);
  tmp[0] &= 0xE7;
  tmp[0] |= 0x18;
  MG_Write_Buffer(0x18, tmp, 1);

  MG_Read_Buffer(0x19, tmp, 2);
  tmp[2] = 0xf0;
  MG_Write_Buffer(0x19, tmp, 3);

  MG_Write_Reg(WAKEUP_NOW, 1);

  Delay_ms(150);

  tmp[0] = 0x08;
  for (i = 0; i < 18; i++)
  {
    tmp[1] = i + 1;
  }
  MG_Write_Buffer(PA_CFG, tmp, 2);

  MG_Read_Buffer(0x19, tmp, 2);
  while (1)
  {
    for (i = 0; i < sizeof(PayLoad); i++)
    {
      tmp[2] = PayLoad[i];
      MG_Write_Buffer(0x19, tmp, 3);
    }
    // if (GetRxTimeout() == FALSE)
    //   break; //received data,break
  }
}
/*******************************************************************************
 * Function    :SRRC_PRBS9Test_NoInit
 * Brief       :
 * param        {*}
 * return       {*}
 * Description :
 * Date        : 2021-07-22 17:55:42
 ******************************************************************************/
// static unsigned char Patch_Data3_V41[4] = {0xb0, 0xff, 0x03, 0x55};
// void SRRC_PRBS9Test_NoInit(unsigned char chan, unsigned char pkt_pld, unsigned char pkt_len)
// {
//   u8 tmp[5];

//   MG_Write_Reg(XO_PD_EN, 0x00);

//   Delay_ms(5);

//   tmp[0] = 0x40;
//   tmp[1] = 0x00;
//   tmp[2] = 0x00;
//   tmp[3] = 0x01;
//   MG_Write_Buffer(ADV_INTERV_RC_CFG, tmp, 4);

//   tmp[0] = 0x01;
//   tmp[1] = 0x01;
//   MG_Write_Buffer(0x35, tmp, 2);

//   tmp[0] = 0x11;
//   tmp[1] = 0x0c;
//   tmp[2] = 0x08;
//   tmp[3] = 0x0c;
//   tmp[4] = 0x11;
//   MG_Write_Buffer(0x3b, tmp, 5);

//   tmp[0] = 0xc0;
//   tmp[1] = 0x0e;
//   tmp[2] = 0x33;
//   tmp[3] = 0x55;
//   MG_Write_Buffer(ABUS_ACC, tmp, 4);

//   tmp[0] = 0xc1;
//   tmp[1] = 0x74;
//   tmp[2] = 0x33;
//   tmp[3] = 0x55;
//   MG_Write_Buffer(ABUS_ACC, tmp, 4);

//   tmp[0] = 0xc4;
//   tmp[1] = 0xe0;
//   tmp[2] = 0x33;
//   tmp[3] = 0x55;
//   MG_Write_Buffer(ABUS_ACC, tmp, 4);

//   MG_Read_Buffer(BG_CAL, tmp, 4);
//   if (ValBit(tmp[3], 5))
//   {
//     Patch_Data3_V41[1] = ((tmp[3] & 0x1f) << 2) | 0x83;
//   }
//   MG_Write_Buffer(ABUS_ACC, Patch_Data3_V41, 4);

//   tmp[0] = 0xc2;
//   if (chan < 11)
//   {
//     tmp[1] = 0x9c;
//   }
//   else if (chan < 21)
//   {
//     tmp[1] = 0x98;
//   }
//   else if (chan < 33)
//   {
//     tmp[1] = 0x94;
//   }
//   else
//   {
//     tmp[1] = 0x90;
//   }
//   tmp[2] = 0x33;
//   tmp[3] = 0x55;
//   MG_Write_Buffer(ABUS_ACC, tmp, 4);

//   tmp[0] = 0x08;
//   tmp[1] = 0xc0;
//   tmp[2] = 0x80 | chan;
//   tmp[3] = 0x00;
//   tmp[4] = 0x80 | pkt_pld;
//   MG_Write_Buffer(CONT_TEST_MODE, tmp, 5);

//   tmp[0] = 0x03;
//   tmp[1] = 0x02;
//   MG_Write_Buffer(XO_SET, tmp, 2);

//   tmp[0] = 0xa0;
//   tmp[1] = 0x20;
//   MG_Write_Buffer(BG_CAL, tmp, 2);

//   MG_Write_Reg(0x30, 0x08);

//   tmp[0] = 0x00;
//   tmp[1] = 0x82;
//   MG_Write_Buffer(0x31, tmp, 2);

//   MG_Write_Reg(0x3e, 0x00);

//   tmp[0] = 0x03;
//   tmp[1] = 0x02;
//   MG_Write_Buffer(XO_SET, tmp, 2);

//   tmp[0] = 0x08;
//   tmp[1] = BLE_TX_POWER;
//   MG_Write_Buffer(PA_CFG, tmp, 2);

//   tmp[0] = 0x00;
//   tmp[1] = 0x86;
//   MG_Write_Buffer(0x31, tmp, 2);

//   tmp[0] = 0x00;
//   tmp[1] = 0x96;
//   MG_Write_Buffer(0x31, tmp, 2);

//   tmp[0] = 0x00;
//   tmp[1] = 0x9e;
//   MG_Write_Buffer(0x31, tmp, 2);

//   tmp[0] = 0x83;
//   tmp[1] = AA_BQB_DTM[0];
//   tmp[2] = AA_BQB_DTM[1];
//   tmp[3] = AA_BQB_DTM[2];
//   tmp[4] = AA_BQB_DTM[3];
//   MG_Write_Buffer(CONFIG, tmp, 5);

//   MG_Write_Reg(WAKEUP_NOW, 1);
// }
//-----------------------------------------------------------
void SRRC_PRBS9Test_NoInit(unsigned char chan,unsigned char power ,unsigned char pkt_pld, unsigned char pkt_len)
{
  u8 AA_BQB_DTM[4] = {0x29, 0x41, 0x76, 0x71};
  unsigned char Patch_Data3_V41[4] = {0xb0, 0xff, 0x03, 0x55};

  u8 tmp[7];

  tmp[0] = 0x01;
  tmp[1] = 0x00;
  MG_Write_Buffer(0x35, tmp, 2);

  tmp[0] = 0x08;
  MG_Write_Buffer(CONT_TEST_MODE, tmp, 1);

  tmp[0] = 5;
  tmp[1] = 0;
  tmp[2] = 0;
  tmp[3] = 0;
  tmp[4] = 0;
  MG_Write_Buffer(0x17, tmp, 5);

  tmp[0] = 0x08;
  tmp[1] = 0x00;
  tmp[2] = 0x19;
  MG_Write_Buffer(ADV_PKT_CFG, tmp, 3);

  tmp[0] = 0x30;
  tmp[1] = 0x00;
  tmp[2] = 0x00;
  tmp[3] = 0x01;
  MG_Write_Buffer(ADV_INTERV_RC_CFG, tmp, 4);

  tmp[0] = 0x03;
  tmp[1] = 0xff;
  tmp[2] = 0xff;
  tmp[3] = 0xff;
  MG_Write_Buffer(0x14, tmp, 4);
  MG_Write_Reg(XO_PD_EN, 0x00);

  Delay_ms(5);
  tmp[0] = 0x01;
  tmp[1] = 0x01;
  MG_Write_Buffer(0x35, tmp, 2);

  tmp[0] = 0x11;
  tmp[1] = 0x0c;
  tmp[2] = 0x08;
  tmp[3] = 0x0c;
  tmp[4] = 0x11;
  MG_Write_Buffer(0x3b, tmp, 5);

  tmp[0] = 0xc0;
  tmp[1] = 0x0e;
  tmp[2] = 0x33;
  tmp[3] = 0x55;
  MG_Write_Buffer(ABUS_ACC, tmp, 4);

  tmp[0] = 0xc1;
  tmp[1] = 0x74;
  tmp[2] = 0x33;
  tmp[3] = 0x55;
  MG_Write_Buffer(ABUS_ACC, tmp, 4);

  tmp[0] = 0xc4;
  tmp[1] = 0xe0;
  tmp[2] = 0x33;
  tmp[3] = 0x55;
  MG_Write_Buffer(ABUS_ACC, tmp, 4);

  MG_Read_Buffer(BG_CAL, tmp, 4);
  if (ValBit(tmp[3], 5))
  {
    Patch_Data3_V41[1] = ((tmp[3] & 0x1f) << 2) | 0x83;
  }
  MG_Write_Buffer(ABUS_ACC, Patch_Data3_V41, 4);

  tmp[0] = 0xc2;
  if (chan < 11)
  {
    tmp[1] = 0x9c;
  }
  else if (chan < 21)
  {
    tmp[1] = 0x98;
  }
  else if (chan < 33)
  {
    tmp[1] = 0x94;
  }
  else
  {
    tmp[1] = 0x90;
  }
  tmp[2] = 0x33;
  tmp[3] = 0x55;
  MG_Write_Buffer(ABUS_ACC, tmp, 4);

  tmp[0] = 0x08;
  tmp[1] = 0x00;
  tmp[2] = 0x80 | chan;
  tmp[3] = 0x00;
  tmp[4] = 0x80 | pkt_pld;
  MG_Write_Buffer(CONT_TEST_MODE, tmp, 5);

  tmp[0] = 0x03;
  tmp[1] = 0x02;
  MG_Write_Buffer(XO_SET, tmp, 2);

  tmp[0] = 0xa0;
  tmp[1] = 0x20;
  MG_Write_Buffer(BG_CAL, tmp, 2);

  MG_Write_Reg(0x30, 0x08);

  tmp[0] = 0x00;
  tmp[1] = 0x82;
  MG_Write_Buffer(0x31, tmp, 2);

  MG_Write_Reg(0x3e, 0x00);

  tmp[0] = 0x03;
  tmp[1] = 0x02;
  MG_Write_Buffer(XO_SET, tmp, 2);

  tmp[0] = 0x08;
  // tmp[1] = BLE_TX_POWER;
  tmp[1] = power;
  MG_Write_Buffer(PA_CFG, tmp, 2);

  tmp[0] = 0x00;
  tmp[1] = 0x86;
  MG_Write_Buffer(0x31, tmp, 2);

  tmp[0] = 0x00;
  tmp[1] = 0x96;
  MG_Write_Buffer(0x31, tmp, 2);

  tmp[0] = 0x00;
  tmp[1] = 0x9e;
  MG_Write_Buffer(0x31, tmp, 2);

  tmp[0] = 0x83;
  tmp[1] = AA_BQB_DTM[0];
  tmp[2] = AA_BQB_DTM[1];
  tmp[3] = AA_BQB_DTM[2];
  tmp[4] = AA_BQB_DTM[3];
  MG_Write_Buffer(CONFIG, tmp, 5);

  MG_Write_Reg(WAKEUP_NOW, 1);
}
