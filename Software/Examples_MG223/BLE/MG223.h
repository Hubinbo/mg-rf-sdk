#ifndef _BLE_H_
#define _BLE_H_

/* set BLE TX power */
#define BLE_TX_POWER_3dbm 10
#define BLE_TX_POWER0dbm 18
#define BLE_TX_POWER1dbm 21
#define BLE_TX_POWER2dbm 25
#define BLE_TX_POWER3dbm 30
#define BLE_TX_POWER6dbm 31

#define BLE_TX_POWER BLE_TX_POWER0dbm

/*interval max=0xffffff, ~524s or 8.7m*/
#define CNT_RC_1MS 32UL
#define CNT_ADV_INTERV 30 // 广播间隔,ms
#define CNT_ADV_INTERV_Data (CNT_RC_1MS * CNT_ADV_INTERV)

#define POWER_SAVE_SLEEP 0
#define POWER_SAVE_STANDBY 1

// #define POWER_SAVE_OPT POWER_SAVE_STANDBY

// INT
// #define INT_TYPE_WAKEUP   	0x20
// #define INT_TYPE_SLEEP    	0x10

#define LF_TIME1MS 32

#define MAP_CHANNEL_37_BIT (0x04 << 5)
#define MAP_CHANNEL_38_BIT (0x02 << 5)
#define MAP_CHANNEL_39_BIT (0x01 << 5)

#define DISABLE_LOCK_DATA_BIT 0x08
#define ADV_EXTEND_IND_BIT 0x01
#define AUX_ADV_IND_BIT 0x02
#define AUX_CHAIN_IND_BIT 0x04
#define AUX_SYNC_IND_BIT 0x08

#define HDR_FLAG_ADVA 0x01
#define HDR_FLAG_TARGETADDR 0x02
#define HDR_FLAG_CTEINFO 0x04
#define HDR_FLAG_ADI 0x08
#define HDR_FLAG_AUXPTR 0x10
#define HDR_FLAG_SYNCINFO 0x20
#define HDR_FLAG_TXPOWER 0x40

#define MG_TYPE_ADV_EXT_IND 0x07
#define TYPE_ADV_EXT_IND 0x07

#define TX_ADDR_TYPE_PUBLIC 0x00
#define TX_ADDR_TYPE_RANDOM 0x40
#define RX_ADDR_TYPE_RANDOM 0x80

#define TX_ADDR_TYPE TX_ADDR_TYPE_RANDOM /*TX_ADDR_TYPE_PUBLIC*/

#define MG_ADV_PHY_1M 0x00
#define MG_ADV_PHY_500K 0x01
#define MG_ADV_PHY_125K 0x02
#define MG_ADV_PHY_2M 0x03

#define AUX_ADV_IND_HOP 0x05
#define AUX_CHAIN_IND_HOP 0x07
#define MG_ADV_CH_AUTO 0x80
/*-------------------------------BLE register---------------------------------*/
// TX Data Payload maxLen 31 Bytes
#define R_TX_PAYLOAD 0xE0
#define W_TX_PAYLOAD 0xE1

#define CONFIG 0x00
#define CONT_TEST_MODE 0x01
#define WAKEUP_NOW 0x02
#define ADV_PKT_CFG 0x03
#define BLE_ADVA 0x04
#define TARGET_ADDRESS 0x05
#define EXT_HEADER_CFG1 0x06   /*Ext header pkt 1/2/3*/
#define EXT_HEADER_CFG2 0x07   /*ADI 1/2/3, CTE*/
#define EXT_HEADER_CFG3 0x08   /*AuxPtr1/2, TxPower1/2/3, 38,39offset??*/
#define EXT_HEADER_CFG4 0x09   /*ACAD2*/
#define EXT_HEADER_CFG5 0x0A   /*ACAD3*/
#define SYNC_INFO_CFG1 0x0B    /*sync info*/
#define SYNC_INFO_CFG2 0x0C    /*sync infor-counter*/
#define WR_SYNC_PKT_STATE 0x0D // to be changed....
#define OFFSET_RC_CFG1 0x0E    /*aux offset rc1/2, step, aux-hop*/
#define OFFSET_RC_CFG2 0x0F    /*aux offset rc3/4*/
#define OFFSET_RC_CFG3 0x10    /*aux offset rc5/6*/
#define OFFSET_RC_CFG4 0x11    /*aux offset rc7/8*/
#define SYNC_INTERVAL_RC_CFG 0x12
#define ADV_INTERV_RC_CFG 0x13
#define BASEBAND_CFG 0x14
#define XO_PD_EN 0x18
// Chip ok
#define CHIP_READY 0x18
#define INT_MASK 0x1C
#define INT_FLAG 0x1D
#define IC_VER 0x1E
#define AUX_OFFSET2_8 0x20
#define RESET_BB 0x21
#define XO_WAIT 0x25
#define PA_CFG 0x2F
#define BG_CAL 0x33
#define XO_SET 0x34
#define ABUS_ACC 0x37

#define RC_CAL_REG 0x39

#define R_TX_FIFO0 0xE0
#define W_TX_FIFO0 0xE1
#define R_TX_FIFO1 0xE2
#define W_TX_FIFO1 0xE3
#define R_TX_FIFO2 0xE4
#define W_TX_FIFO3 0xE5
/*-------------------------------BLE register  End----------------------------*/

/*
15:8:	Pdu length
7:0:	Pdu type
*/
#define ADV_IND 0
#define ADV_DIRECT_IND 1
#define ADV_NONCONN_IND 2
// #define ADV_SCAN_REQ       3
// #define ADV_SCAN_RSP       4
// #define ADV_CONN_REQ       5
#define ADV_SCAN_IND 6

// 0x40: Random, 0x00: Public
#define ADV_TXADD_PUBLIC 0x00
#define ADV_TXADD_RANDOM 0x40

// Nordic SDK GAP  AD_TYPE
#define BLE_GAP_AD_TYPE_FLAGS 0x01                              /**< Flags for discoverability. */
#define BLE_GAP_AD_TYPE_16BIT_SERVICE_UUID_MORE_AVAILABLE 0x02  /**< Partial list of 16 bit service UUIDs. */
#define BLE_GAP_AD_TYPE_16BIT_SERVICE_UUID_COMPLETE 0x03        /**< Complete list of 16 bit service UUIDs. */
#define BLE_GAP_AD_TYPE_32BIT_SERVICE_UUID_MORE_AVAILABLE 0x04  /**< Partial list of 32 bit service UUIDs. */
#define BLE_GAP_AD_TYPE_32BIT_SERVICE_UUID_COMPLETE 0x05        /**< Complete list of 32 bit service UUIDs. */
#define BLE_GAP_AD_TYPE_128BIT_SERVICE_UUID_MORE_AVAILABLE 0x06 /**< Partial list of 128 bit service UUIDs. */
#define BLE_GAP_AD_TYPE_128BIT_SERVICE_UUID_COMPLETE 0x07       /**< Complete list of 128 bit service UUIDs. */
#define BLE_GAP_AD_TYPE_SHORT_LOCAL_NAME 0x08                   /**< Short local device name. */
#define BLE_GAP_AD_TYPE_COMPLETE_LOCAL_NAME 0x09                /**< Complete local device name. */
#define BLE_GAP_AD_TYPE_TX_POWER_LEVEL 0x0A                     /**< Transmit power level. */
#define BLE_GAP_AD_TYPE_CLASS_OF_DEVICE 0x0D                    /**< Class of device. */
#define BLE_GAP_AD_TYPE_SIMPLE_PAIRING_HASH_C 0x0E              /**< Simple Pairing Hash C. */
#define BLE_GAP_AD_TYPE_SIMPLE_PAIRING_RANDOMIZER_R 0x0F        /**< Simple Pairing Randomizer R. */
#define BLE_GAP_AD_TYPE_SECURITY_MANAGER_TK_VALUE 0x10          /**< Security Manager TK Value. */
#define BLE_GAP_AD_TYPE_SECURITY_MANAGER_OOB_FLAGS 0x11         /**< Security Manager Out Of Band Flags. */
#define BLE_GAP_AD_TYPE_SLAVE_CONNECTION_INTERVAL_RANGE 0x12    /**< Slave Connection Interval Range. */
#define BLE_GAP_AD_TYPE_SOLICITED_SERVICE_UUIDS_16BIT 0x14      /**< List of 16-bit Service Solicitation UUIDs. */
#define BLE_GAP_AD_TYPE_SOLICITED_SERVICE_UUIDS_128BIT 0x15     /**< List of 128-bit Service Solicitation UUIDs. */
#define BLE_GAP_AD_TYPE_SERVICE_DATA 0x16                       /**< Service Data - 16-bit UUID. */
#define BLE_GAP_AD_TYPE_PUBLIC_TARGET_ADDRESS 0x17              /**< Public Target Address. */
#define BLE_GAP_AD_TYPE_RANDOM_TARGET_ADDRESS 0x18              /**< Random Target Address. */
#define BLE_GAP_AD_TYPE_APPEARANCE 0x19                         /**< Appearance. */
#define BLE_GAP_AD_TYPE_ADVERTISING_INTERVAL 0x1A               /**< Advertising Interval. */
#define BLE_GAP_AD_TYPE_LE_BLUETOOTH_DEVICE_ADDRESS 0x1B        /**< LE Bluetooth Device Address. */
#define BLE_GAP_AD_TYPE_LE_ROLE 0x1C                            /**< LE Role. */
#define BLE_GAP_AD_TYPE_SIMPLE_PAIRING_HASH_C256 0x1D           /**< Simple Pairing Hash C-256. */
#define BLE_GAP_AD_TYPE_SIMPLE_PAIRING_RANDOMIZER_R256 0x1E     /**< Simple Pairing Randomizer R-256. */
#define BLE_GAP_AD_TYPE_SERVICE_DATA_32BIT_UUID 0x20            /**< Service Data - 32-bit UUID. */
#define BLE_GAP_AD_TYPE_SERVICE_DATA_128BIT_UUID 0x21           /**< Service Data - 128-bit UUID. */
#define BLE_GAP_AD_TYPE_3D_INFORMATION_DATA 0x3D                /**< 3D Information Data. */
#define BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA 0xFF         /**< Manufacturer Specific Data. */

#define Manufacturer_ID1 0x9c
#define Manufacturer_ID2 0x05 // 厂商ID-MacroGiga Electronics

#define GAP_ADTYPE_FLAGS_LIMITED 0x01             //!< Discovery Mode: LE Limited Discoverable Mode
#define GAP_ADTYPE_FLAGS_GENERAL 0x02             //!< Discovery Mode: LE General Discoverable Mode
#define GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED 0x04 //!< Discovery Mode: BR/EDR Not Supported

extern u8 ble_ver;
extern u8 ble_Addr[];
extern u8 txgain_r;
extern u8 txgain_w;

extern u8 g_pkt_types;
extern u8 g_adv_data_pkt2[];
extern u8 g_adv_data_pkt2_len;
extern u8 g_adv_data_pkt3[];
extern u8 g_adv_data_pkt3_len;

extern void BLE_RESET(void);
extern void BLE_Init(u8 TxPower, u8 debug);
extern void BLE_TX(void);
extern void BLE_Mode_Sleep(void);
extern void BLE_Mode_Stdby(void);
extern void BLE_upDte(void);

extern void BLE_TX_Ext(void);
extern u8 radio_getIrqState(void);
extern void update_adv_ext_ind_par(void);
extern void update_aux_chain_ind_par(void);
extern void update_aux_sync_ind_par(void);
extern void update_adv_ext_ind_data(unsigned char *data, unsigned char len);
extern void update_aux_chain_ind_data(unsigned char *data, unsigned char len);

#endif
