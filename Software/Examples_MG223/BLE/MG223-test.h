/*******************************************************************************
 * FilePath    : /Software/Examples_MG223/BLE/MG223-test.h
 * Version     : 1.0
 * Brief       :
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2021-04-16 10:31:34
 * LastEditors : AndrewHu
 * LastEditTime: 2022-06-08 18:07:31
 * Description :
 ******************************************************************************/
#ifndef _MG223_TEST_H_
#define _MG223_TEST_H_
/* Includes ------------------------------------------------------------------*/
#include <Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define PKT_PAYLOAD_PRBS9 0
#define PKT_PAYLOAD_11110000 1
#define PKT_PAYLOAD_10101010 2
#define PKT_PAYLOAD_PRBS15 3
#define PKT_PAYLOAD_11111111 4
#define PKT_PAYLOAD_00000000 5
#define PKT_PAYLOAD_00001111 6
#define PKT_PAYLOAD_01010101 7
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/*******************************************************************************
 * Function    : SRCC_CarrierTest
 * Brief       : 载波测试
 * Parameter   : {unsigned char} chan(0-39)
 * Returns     :
 * Date        : 2021-04-16 19:30:12
 * Description :
 ******************************************************************************/
void SRCC_CarrierTest(unsigned char chan);
/*******************************************************************************
 * Function    : SRRC_PRBS9Test
 * Brief       : 调制波测试
 * Parameter   : {unsigned char} chan
 * Parameter   : {unsigned char} pkt_pld
 * Parameter   : {unsigned char} pkt_len
 * Returns     :
 * Date        : 2021-04-16 19:29:14
 * Description :
 ******************************************************************************/
void SRRC_PRBS9Test(unsigned char chan, unsigned char pkt_pld, unsigned char pkt_len);
void SRRC_PRBS9Test_sdk(unsigned char chan, unsigned char pkt_pld, unsigned char pkt_len);
// void SRRC_PRBS9Test_NoInit(unsigned char chan, unsigned char pkt_pld, unsigned char pkt_len);
void SRRC_PRBS9Test_NoInit(unsigned char chan, unsigned char power ,unsigned char pkt_pld, unsigned char pkt_len);
/*******************************************************************************
 * Function    : RF_Test
 * Brief       :
 * param        {unsigned char} chan
 * param        {unsigned char} pkt_pld
 * param        {unsigned char} pkt_len
 * return       {*}
 * Description :
 * Date        : 2021-07-22 18:02:18
 ******************************************************************************/
void RF_Test(unsigned char chan, unsigned char pkt_pld, unsigned char pkt_len);
/* extern --------------------------------------------------------------------*/

#endif
