/*******************************************************************************
 * FilePath    : \Software\Examples_MG223\MG223_app_Tx\Inc\Includes.h
 * Version     : 1.0
 * Brief       : 
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2020-03-24 15:56:39
 * LastEditors : AndrewHu
 * LastEditTime: 2021-04-16 14:12:31
 * Description : 
 ******************************************************************************/
#ifndef __INCLUDES_H__
#define __INCLUDES_H__
/* Includes ------------------------------------------------------------------*/
// #include <stdint.h>
#include <string.h>
#include <stdio.h>
/* Private typedef -----------------------------------------------------------*/
// typedef uint32_t u32;
// typedef uint16_t u16;
// typedef uint8_t u8;
/* Private define ------------------------------------------------------------*/
#include "stm8l10x_conf.h"
#include "HardwareCFG.h"

#include "AH_Lib_Includes.h"
#include "MG223.h"

#include "BSP.h"
#include "I2C.h"
#include "INT.h"
#include "SPI.h"
#include "UART.h"

#include "User_Printf.h"
#include "User_Key.h"
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/* extern --------------------------------------------------------------------*/

#endif
