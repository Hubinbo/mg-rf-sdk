/*******************************************************************************
 * FilePath    : /Software/Examples_MG223/MG223_app_Tx/main.c
 * Version     : 1.0
 * Brief       :
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2020-03-24 15:42:28
 * LastEditors : AndrewHu
 * LastEditTime: 2022-06-09 09:56:16
 * Description :
 ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define StandbyMode 0x01        //停止工作，进入standby
#define SleepMode 0x02          //停止工作，进入sleep
#define NormalMode 0x03         //正常模式(RF Sleep)
#define NormalModed_upDate 0x04 //正常模式(RF Sleep),更新数据
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
u8 runningMode = NormalMode; //
u8 UartRxData[50];
u8 UartRxDataLen;
/* Private function ----------------------------------------------------------*/
/* extern --------------------------------------------------------------------*/
extern u8 POWER_SAVE_OPT;
/*******************************************************************************
 * Function    : Set_RuningMOde
 * Brief       :
 * Parameter   :
 * Returns     :
 * Date        : 2021-04-14 12:17:36
 * Description : 设置工作模式
 ******************************************************************************/
void Set_RuningMOde(u8 mode)
{
    // standby模式只能通过复位脚复位唤醒
    if (POWER_SAVE_OPT == POWER_SAVE_STANDBY)
        BLE_RESET();

    switch (mode)
    {
    case StandbyMode: // rf not work
    {
        runningMode = StandbyMode;
        POWER_SAVE_OPT = POWER_SAVE_STANDBY;

        BLE_Mode_Stdby();
#if (COMM_OPT_SEL == COMM_I2C_SIMU_NOPU)
        GPIO_Init(BLE_SCL_PORT, BLE_SCL, GPIO_Mode_Out_OD_HiZ_Slow);
        GPIO_Init(BLE_SDA_PORT, BLE_SDA, GPIO_Mode_Out_OD_HiZ_Slow);
#endif
        LED_RED_OFF();
        LED_GREEN_ON();
        Printf_NewLine();
        Uart_Send_String("RunningMode: Standby", 20);
        Printf_NewLine();
    }
    break;
    case SleepMode: // rf not work
    {
        runningMode = SleepMode;
        POWER_SAVE_OPT = POWER_SAVE_SLEEP;

        BLE_Init(BLE_TX_POWER, FALSE);

        BLE_Mode_Sleep();
#if (COMM_OPT_SEL == COMM_I2C_SIMU_NOPU)
        GPIO_Init(BLE_SCL_PORT, BLE_SCL, GPIO_Mode_Out_OD_HiZ_Slow);
        GPIO_Init(BLE_SDA_PORT, BLE_SDA, GPIO_Mode_Out_OD_HiZ_Slow);
#endif
        LED_RED_ON();
        LED_GREEN_OFF();
        Printf_NewLine();
        Uart_Send_String("RunningMode: Sleep", 18);
        Printf_NewLine();
    }
    break;
    case NormalMode: // rf running
    {
        runningMode = NormalMode;
        POWER_SAVE_OPT = POWER_SAVE_SLEEP;

        BLE_Init(BLE_TX_POWER, FALSE); //休眠模式下唤醒工作，要先初始

        BLE_TX();

        LED_RED_ON();
        LED_GREEN_ON();
        Printf_NewLine();
        Uart_Send_String("RunningMode: Normal", 19);
        Printf_NewLine();
    }
    break;
    default: // rf updata
    {
        //只更新数据
        runningMode = NormalModed_upDate;
        POWER_SAVE_OPT = POWER_SAVE_SLEEP;

        BLE_upDte(); //先停止广播，然后更新数据，再使能广播

        LED_RED_ON();
        LED_GREEN_ON();
        Printf_NewLine();
        Uart_Send_String("RunningMode: upDate", 19);
        Printf_NewLine();
    }
    break;
    }
}
/*******************************************************************************
 * Function    : KeyTouch_Event
 * Brief       :
 * Parameter   :
 * Returns     :
 * Date        : 2021-04-14 14:38:28
 * Description : 按键触发事件处理
 ******************************************************************************/
void KeyTouch_Event(void)
{
    static u8 mode = 0x00;

    mode++;
    switch (mode)
    {
    case 1:
        Set_RuningMOde(StandbyMode);
        break;
    case 2:
        Set_RuningMOde(NormalMode);
        break;
    case 3:
        Set_RuningMOde(SleepMode);
        break;
    case 4:
        Set_RuningMOde(NormalMode);
        break;
    case 5:
        Set_RuningMOde(NormalModed_upDate);
        break;
    default:
        mode = 0x00;
        Set_RuningMOde(NormalMode);
        break;
    }
}
/*******************************************************************************
 * Function    : main
 * Brief       :
 * Parameter   :
 * Returns     :
 * Date        : 2020-03-24 16:11:19
 * Description : 启动函数，主循环
 ******************************************************************************/
int main(void)
{
    u32 time;

    Init_System();

    Printf_PowerUp(); //上电信息打印

    if (POWER_SAVE_OPT == POWER_SAVE_STANDBY)
        BLE_RESET();

    BLE_Init(BLE_TX_POWER, TRUE); //蓝牙初始化
    Uart_Send_String("BLE init ok.\r\n", 14);

    Set_RuningMOde(NormalMode);

    time = GetSysTickCount();
    while (1)
    {
        if ((GetSysTickCount() - time) >= 5)
        { // 5ms
            time = GetSysTickCount();
            Key_Scan();
        }

        //串口示例，收到数据直接打印
        Uart_Recived(UartRxData, &UartRxDataLen);
        if (UartRxDataLen != 0)
        {
            Uart_Send_String(UartRxData, UartRxDataLen);
            UartRxDataLen = 0;
        }
    }
}
