/*******************************************************************************
 * FilePath    : \Software\Examples_MG223\MG223_app_Tx\Src\User\User_Key.c
 * Version     : 1.0
 * Brief       : 
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2021-04-13 17:13:21
 * LastEditors : AndrewHu
 * LastEditTime: 2021-04-16 14:14:06
 * Description : 
 ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "Includes.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/* extern --------------------------------------------------------------------*/
/*******************************************************************************
 * Function    : Key_Scan
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-14 14:38:28
 * Description : 按键扫描，unit-5ms
 ******************************************************************************/
void Key_Scan(void)
{
    static u8 key_delay = 0x00;
    static u8 key_flag = 0x00;
    if (KEY_GET() == RESET)
    { //按下
        if (key_flag == 0x00)
            key_delay = 0x00;
    }
    else
    { //松开
        if (key_flag != 0x00)
            key_delay = 0x00;
    }

    key_delay++;
    if (key_delay < 10)
        return; //去抖
    key_delay = 0x00;

    if (key_flag == 0x00)
    {
        key_flag = TRUE;
        //按键松开
    }
    else
    {
        key_flag = FALSE;
        //按键按下
        KeyTouch_Event();
    }
}
