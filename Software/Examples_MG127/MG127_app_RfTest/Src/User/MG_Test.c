/*******************************************************************************
 * FilePath    : /MG127_app_RfTest/Src/User/MG_Test.c
 * Version     : 1.0
 * Brief       : 
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2021-04-16 10:31:34
 * LastEditors : AndrewHu
 * LastEditTime: 2021-05-13 15:31:20
 * Description : 
 ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "Includes.h"

// extern uint32_t systick;
// extern uint8_t ble_ver;
// extern uint8_t ble_Addr[];
// extern uint8_t txgain_r;
// extern uint8_t txgain_w;

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define testDevice "\r\nDevice: MG127"      //测试芯片型号
#define testDeviceVer "\r\nDevice Ver: "    //硬件版本信息
#define testDeviceMacAddr "\r\nMac Addr: "  //硬件版本信息
#define testSoftVer "\r\nSoftware Ver: 2.2" //测试软件版本信息
#define testTxgainR "\r\nR: "               //
#define testTxgainW "\r\nW: "               //

#define CmdHeader "MG:"
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
u8 RxData[18] = {0}; //数据内容
u8 RxDataLen = 0x00; //数据长度
/* Private function prototypes -----------------------------------------------*/
u32 test_timer = 0x00; //单位计时
u8 time_unit = 0x00;   //单位时间累计

u8 test_data[12] = {0};     //串口数据保存
u8 test_txPower = 0x00;     //发射功率
u8 test_channel = 0x00;     //通道选择
u8 test_chanl_map[5] = {0}; //通道列表
u8 test_map_num = 0x00;     //通道编号(0~40)
/* Private function prototypes -----------------------------------------------*/
/* extern --------------------------------------------------------------------*/

/*******************************************************************************
* Function   :      test_power_set
* Parameter  :      uint8_t power
* Returns    :      void
* Description:
* Note:      :
*******************************************************************************/
void test_power_set(u8 *txPower)
{
    switch (*txPower)
    {
    case 0x01:
        test_txPower = BLE_TX_POWER3dbm;
        break;
    case 0x02:
        test_txPower = BLE_TX_POWER0dbm;
        break;
    case 0x03:
        test_txPower = BLE_TX_POWER_3dbm;
        break;
    case 0x04:
        test_txPower = BLE_TX_POWER_6dbm;
        break;
    case 0x05:
        test_txPower = BLE_TX_POWER_8dbm;
        break;
    case 0x06:
        test_txPower = BLE_TX_POWER_15dbm;
        break;
    case 0x07:
        test_txPower = BLE_TX_POWER_20dbm;
        break;
    case 0x08:
        test_txPower = BLE_TX_POWER_30dbm;
        break;
    case 0x09:
        test_txPower = BLE_TX_POWER_37dbm;
        break;
    case 0x0a:
        test_txPower = BLE_TX_POWER_54dbm;
        break;
    default:
        test_txPower = BLE_TX_POWER0dbm; //默认0db
        *txPower = 0x02;
        Printf_Error(ErrorCode_TxPower);
        Uart_Send_String((u8 *)"\r\nSet default 0dbm", 18);
        break;
    }
}
/*******************************************************************************
 * Function    : test_channel_map_save
 * Brief       : 跳频列表保存
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 17:54:36
 * Description : 
 ******************************************************************************/
void test_channel_map_save(void) //跳频表
{
    AHL_Data_Memcpy(test_chanl_map, &test_data[4], 5);
    test_map_num = 0x00;
}
/*******************************************************************************
 * Function    : test_channel_set
 * Brief       : 通道选择(通道参数值)
 * Parameter   : 
 * Returns     : test_channel-待测试的通道
 * Date        : 2021-04-16 18:28:37
 * Description : 
 * param {u8} chan
 ******************************************************************************/
void test_channel_set(u8 chan)
{
    if (chan >= 40)
    {
        test_channel = 0x02;
        // test_channel = 0x00;
    }
    else
    {
        test_channel = (chan + 1) << 1;
        // test_channel = chan;
    }
}
/*******************************************************************************
 * Function    : testf_channel_remove
 * Brief       : 通道列表位移
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 18:29:06
 * Description : 
 ******************************************************************************/
void test_channel_remove(void)
{
    test_chanl_map[0] <<= 1;
    if (test_chanl_map[1] & 0x80)
        test_chanl_map[0]++;

    test_chanl_map[1] <<= 1;
    if (test_chanl_map[2] & 0x80)
        test_chanl_map[1]++;

    test_chanl_map[2] <<= 1;
    if (test_chanl_map[3] & 0x80)
        test_chanl_map[2]++;

    test_chanl_map[3] <<= 1;
    if (test_chanl_map[4] & 0x80)
        test_chanl_map[3]++;

    test_chanl_map[4] <<= 1;
}
/*******************************************************************************
 * Function    : test_channel_run
 * Brief       : 跳频通道设定
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 18:29:54
 * Description : 
 ******************************************************************************/
void test_channel_run(void)
{
    //test_chanl_map[2~6]
    while ((test_chanl_map[0] & 0x80) == 0x00)
    { //查找需要发射(接收)的通道
        test_channel_remove();
        test_map_num++;
        if (test_map_num >= 40)
        {
            test_channel_map_save();
            return; //没有找到通道内容，维持原通道
        }
    }

    //根据通道map，找到通道
    test_channel_set(test_map_num);

    //准备下一次位移
    test_channel_remove();
    test_map_num++;
    if (test_map_num >= 40)
    {
        test_channel_map_save();
    }
}
/*******************************************************************************
* Function   :      Test_Mode_Certification
* Parameter  :      void
* Returns    :      void
* Description:
* Note:      :      认证测试执行
*******************************************************************************/
void Test_Mode_Certification(void)
{
    if ((GetSysTickCount() - test_timer) < 100) //100ms
        return;
    test_timer = GetSysTickCount();

    time_unit++; //单通道持续时间累计
    if (time_unit < test_data[2])
        return;
    time_unit = 0x00;

    switch (test_data[1])
    {
    case (0x01):
        //单载波发射模式
        if (test_data[2] != 0x00)
        {
            test_power_set(&test_data[3]); //发射功率
            BLE_Init(test_txPower, FALSE);
            test_channel_run();
            SRCCCarrierTest(test_channel, test_txPower);
        }
        test_data[2] = 0x00; //单次发射，下次不再发射
        break;
    case (0x02):
        //载波跳频模式
        test_power_set(&test_data[3]); //发射功率
        BLE_Init(test_txPower, FALSE);
        test_channel_run();
        SRCCCarrierTest(test_channel, test_txPower);
        break;
    case (0x03):
        //单调制发射模式
        if (test_data[2] != 0x00)
        {
            test_power_set(&test_data[3]); //发射功率
            BLE_Init(test_txPower, FALSE);
            test_channel_run();
            SRRC_PRBS9Test(test_channel, test_txPower);
        }
        test_data[2] = 0x00;
        break;
    case (0x04):
        //调制跳频模式
        test_power_set(&test_data[3]); //发射功率
        BLE_Init(test_txPower, FALSE);
        test_channel_run();
        SRRC_PRBS9Test(test_channel, test_txPower);
        break;
    case (0x05):
        //跳频，接收
        test_power_set(&test_data[3]); //发射功率
        BLE_Init(test_txPower, FALSE);
        test_channel_run();
        RXTest(test_channel);
        break;
    default:
        break;
    }
}
// /*******************************************************************************
// * Function   :      test_Mode_function
// * Parameter  :      void
// * Returns    :      void
// * Description:
// * Note:      :      芯片功能测试
// *******************************************************************************/
// void test_Mode_function(void)
// {
//     if ((systick - test_tick) < 10) //10ms
//         return;
//     test_tick = systick;
//     time_unit++;
//     if (time_unit < test_data8) //收、发射间隔
//         return;
//     time_unit = 0x00;

//     if (test_data6 == 0x00) //收发次数
//         return;

//     if (test_data6 != 0xff) //判断是否为连续收发（不记次数）
//         test_data6--;

//     txcnt = 3; //txcnt=0 is for rx only application
//     rxcnt = 0; //MG123没有发射功能

//     if (test_data5 == 0x02)
//         txcnt = 0; //仅接收

//     BLE_TRX();
// }
// /*******************************************************************************
//  * Function    : Test_Mode_Set_Special
//  * Brief       : 特殊测试模式设置
//  * Parameter   :
//  * Returns     :
//  * Date        : 2020-12-29 15:58:43
//  * Description :
//  ******************************************************************************/
// void Test_Mode_Special(void)
// {

//     if (test_data[1] == 0x01)
//     {
//         //特殊数据发送测试
//         test_power_set(&test_data[3]); //发射功率
//         BLE_Init(test_txPower, FALSE); //初始化
//         test_channel_run();            //通道设置
//         SRRC_PRBS9Test(test_channel, test_data[2], 39);
//         test_data[1] = 0x00;
//     }
// }
/*******************************************************************************
 * Function    : Test_Data_Check
 * Brief       : 数据检查,如果正确则保存数据内容
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 19:57:51
 * Description : 
 ******************************************************************************/
void Test_Data_Check(void)
{
    if (AHL_Data_BlankCheck(&RxData[8], 5))
    { //通道设置为空
        Printf_Error(ErrorCode_Channel);
        return;
    }

    test_power_set(&RxData[7]); //查询发射功率是否正确

    //指令正确，保存指令内容
    AHL_Data_Memcpy(test_data, &RxData[4], 12);
    test_channel_map_save(); //保存跳频(通道)列表

    //第1次立即执行测试内容
    time_unit = 0xfe;
    Printf_OK();
}
/*******************************************************************************
 * Function    : Test_Mode_Set
 * Brief       : 功能选择
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 16:21:55
 * Description : 
 ******************************************************************************/
void Test_Mode_Set(void)
{
    //芯片型号判断
    if ((RxData[3] != 0xff) && (RxData[3] != 0x0d))
    { //芯片选型错误
        Printf_Error(ErrorCode_DeviceType);
    }
    else
    { //功能选择

        switch (RxData[4])
        {
        case 0x01:
        { //芯片功能测试
            test_timer = GetSysTickCount();
            if (RxData[5] != 0x01)
            { //芯片只发射，否则错误
                Printf_Error(ErrorCode_Function);
                return;
            }
            Test_Data_Check();
        }
        break;
        case 0x02:
        { //芯片内部参数测试,打印出各个参数内容
            AHL_Data_Memcpy(test_data, &RxData[4], 12);
            Uart_Send_String(testDevice, sizeof(testDevice) - 1);
            Uart_Send_String(testDeviceVer, sizeof(testDeviceVer) - 1);
            Uart_Send_Byte2Ascii(ble_ver);
            Uart_Send_String(testSoftVer, sizeof(testSoftVer) - 1);
            Uart_Send_String(testDeviceMacAddr, sizeof(testDeviceMacAddr) - 1);
            Uart_Send_Array2Ascii_R(ble_Addr, 6, FALSE);
            Uart_Send_String(testTxgainR, sizeof(testTxgainR) - 1);
            Uart_Send_Byte2Ascii(txgain_r);
            Uart_Send_String(testTxgainW, sizeof(testTxgainW) - 1);
            Uart_Send_Byte2Ascii(txgain_w);
            Printf_OK();
        }
        break;
        case 0x03:
        { //认证测试
            test_timer = GetSysTickCount();
            if ((RxData[5] == 0x00) || (RxData[5] > 0x05))
            { //功能错误
                Printf_Error(ErrorCode_Function);
                return;
            }

            Test_Data_Check();
        }
        break;
        // case 0x04:
        // { //特殊测试
        //     switch (RxData[5])
        //     {
        //     case 0x01:
        //     { //数据发射测试
        //         Test_Data_Check();
        //     }
        //     break;
        //     case 0x02:
        //     { //数据接收测试
        //         Test_Data_Check();
        //     }
        //     break;
        //     default:
        //     { //功能错误
        //         Printf_Error(ErrorCode_Function);
        //     }
        //     break;
        //     }
        // }
        // break;
        default:
        { //未知功能
            Printf_Error(ErrorCode_UnknownCmd);
        }
        break;
        }
    }
}
/*******************************************************************************
 * Function    : Fun_Test_Main
 * Brief       : 芯片测试
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 10:39:04
 * Description : 
 ******************************************************************************/
void Fun_Test_Main(void)
{
    while (1)
    {
        Uart_Recived(RxData, &RxDataLen); //如果Uart_Event= 0x01,有收到指令RxData[]
        if (RxDataLen != 0)
        { //串口有接收到数据
            if (RxDataLen != 16)
            { //数据长度不对
                Printf_Error(ErrorCode_UnknownCmd);
            }
            else if (AHL_Data_Compare((u8 *)CmdHeader, RxData, 3))
            { //引导数据正确
                Test_Mode_Set();
            }
            else
            { //数据引导符错误
                Printf_Error(ErrorCode_UnknownCmd);
            }
            RxDataLen = 0;
        }

        switch (test_data[0])
        {
        case 0x01:
        { //功能测试
            // Test_Mode_Function();
        }
        break;
        case 0x02:
        { //获取信息
        }
        break;
        case 0x03:
        { //认证测试
            Test_Mode_Certification();
        }
        break;
        // case 0x04:
        // { //特殊测试
        //     Test_Mode_Special();
        // }
        // break;
        default:
        { //其他
        }
        break;
        }
    }
}
