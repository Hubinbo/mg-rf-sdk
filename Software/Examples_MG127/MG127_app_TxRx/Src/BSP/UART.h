/*******************************************************************************
 * FilePath    : /Software/Examples_MG223/MG223_app_Tx/Src/BSP/UART.h
 * Version     : 1.0
 * Brief       : 
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2020-04-03 15:06:14
 * LastEditors : AndrewHu
 * LastEditTime: 2021-05-13 10:29:35
 * Description : 
 ******************************************************************************/
#ifndef __UART_H__
#define __UART_H__
/* Includes ------------------------------------------------------------------*/
#include <Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/

/*******************************************************************************
 * Function    : Init_Uart
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-13 17:33:25
 * Description : 
 ******************************************************************************/
void Init_Uart(void);
/*******************************************************************************
* Function   :      UartRx_Int
* Parameter  :      void
* Returns    :      void
* Description:
* Note:      :      串口中断服务程序
*******************************************************************************/
void UartRx_Int(void);
/*******************************************************************************
 * Function    : Uart_Send_Byte
 * Brief       : 
 * Parameter   : data
 * Returns     : void
 * Date        : 2021-04-13 17:33:50
 * Description : 
 ******************************************************************************/
void Uart_Send_Byte(unsigned char data);
/*******************************************************************************
 * Function    : Uart_Send_Byte2Ascii
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-13 17:40:28
 * Description : 
 ******************************************************************************/
void Uart_Send_Byte2Ascii(unsigned char data);
/*******************************************************************************
 * Function    : Uart_Send_Array
 * Brief       : 
 * Parameter   : {u8*} data-待发送数组
 * Parameter   : {u8} len-数组长度
 * Returns     : 
 * Date        : 2020-03-24 18:30:47
 * Description : 发送一个数组（原始数据）
 ******************************************************************************/
void Uart_Send_Array(u8 *data, u8 len);
/*******************************************************************************
 * Function    : Uart_Send_Array_R
 * Brief       : 
 * Parameter   : {u8*} data-待发送数组
 * Parameter   : {u8} len-数组长度
 * Returns     : 
 * Date        : 2020-03-24 18:30:47
 * Description : 发送一个数组（原始数据），倒序
 ******************************************************************************/
void Uart_Send_Array_R(u8 *data, u8 len);
/*******************************************************************************
 * Function    : Uart_Send_Array2Ascii
 * Brief       : 
 * Parameter   : {u8*} data-待发送数组
 * Parameter   : {u8} len-数组长度
 * Parameter   : {u8} space-是否空格做间隔
 * Returns     : 
 * Date        : 2020-03-24 18:30:47
 * Description : 发送一个数组（转成ASCII）
 ******************************************************************************/
void Uart_Send_Array2Ascii(u8 *data, u8 len, u8 space);
/*******************************************************************************
 * Function    : Uart_Send_Array2Ascii_R
 * Brief       : 
 * Parameter   : {u8*} data-待发送数组
 * Parameter   : {u8} len-待发送数组长度
 * Parameter   : {u8} space-是否空格做间隔
 * Returns     : 
 * Date        : 2020-03-24 18:30:47
 * Description : 发送一个数组（转成ASCII），倒序
 ******************************************************************************/
void Uart_Send_Array2Ascii_R(u8 *data, u8 len, u8 space);
/*******************************************************************************
 * Function    : Uart_Send_String
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-13 17:38:41
 * Description : 
 ******************************************************************************/
void Uart_Send_String(u8 *data, u8 len);
/*******************************************************************************
 * Function    : Uart_Recived
 * Brief       : 
 * Parameter   : {u8 *}data-串口接收到的数据类容
 * Parameter   : {u8 *}len-串口接收到的数据长度
 * Returns     : 
 * Date        : 2021-04-16 10:43:26
 * Description : 串口接收信息处理
 ******************************************************************************/
void Uart_Recived(u8 *data, u8 *len);
/*******************************************************************************
 * Function    : GetRxTimeout
 * Brief       : 
 * param        {*}
 * return       {*}
 * Description : 
 * Date        : 2021-05-13 10:07:40
 ******************************************************************************/
u8 GetRxTimeout(void);
/* extern --------------------------------------------------------------------*/
#endif
