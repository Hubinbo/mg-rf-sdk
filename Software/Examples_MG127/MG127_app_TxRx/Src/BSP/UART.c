/*******************************************************************************
 * FilePath    : /Software/Examples_MG223/MG223_app_Tx/Src/BSP/UART.c
 * Version     : 1.0
 * Brief       : 
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2021-04-13 17:13:21
 * LastEditors : AndrewHu
 * LastEditTime: 2021-05-13 10:29:28
 * Description : 
 ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "Includes.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
//Usart config data
#define Usart_BaudRate 115200UL

//USART_WordLength_8D: 8 bits Data
//USART_WordLength_9D: 9 bits Data
#define Usart_Data_Len USART_WordLength_8D

//USART_StopBits_1: One stop bit is transmitted at the end of frame
//USART_StopBits_2: Two stop bits are transmitted at the end of frame
#define Usart_StopBit USART_StopBits_1
//USART_Parity_No: No Parity
//USART_Parity_Even: Even Parity
//USART_Parity_Odd: Odd Parity

#define Usart_Parity USART_Parity_No
//USART_Mode_Rx: Receive Enable
//USART_Mode_Tx: Transmit Enable
#define Usart_Mode (USART_Mode_Rx | USART_Mode_Tx)

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
//set usart buffer len
#define UART_BUF_LEN 50
volatile u8 Rx_Buffer[UART_BUF_LEN] = {0};
volatile u8 Rx_Buffer_Cnt = 0;
u8 Rx_TimeOut = TRUE; //接收超时

/* Private function ----------------------------------------------------------*/
/*******************************************************************************
 * Function    : Init_Uart
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-13 17:33:25
 * Description : 
 ******************************************************************************/
void Init_Uart(void)
{
	//reset usart
	USART_DeInit();
	//enalbe TX RX PULL Resistor
	GPIO_ExternalPullUpConfig(RX_PORT, RX | TX, ENABLE);
	//GPIO_ExternalPullUpConfig(TX_PORT,TX, ENABLE);

	//usart config data
	USART_Init((uint32_t)Usart_BaudRate, Usart_Data_Len, Usart_StopBit, Usart_Parity, (USART_Mode_TypeDef)Usart_Mode);

	USART_ClockInit(USART_Clock_Disable, USART_CPOL_Low, USART_CPHA_2Edge, USART_LastBit_Disable);

	//enable usart rx interrup
	USART_ITConfig(USART_IT_RXNE, ENABLE);

	USART_ITConfig(USART_IT_TXE, DISABLE);
	USART_ITConfig(USART_IT_TC, DISABLE);

	//set RX interrupt Level 1.
	ITC_SetSoftwarePriority(USART_RX_IRQn, ITC_PriorityLevel_3);

	//enable usart function
	USART_Cmd(ENABLE);
}
/*******************************************************************************
* Function   :      UartRx_Int
* Parameter  :      void
* Returns    :      void
* Description:
* Note:      :      串口中断服务程序
*******************************************************************************/
void UartRx_Int(void)
{
	Rx_Buffer[Rx_Buffer_Cnt] = (u8)USART_ReceiveData8();
	Rx_Buffer_Cnt++;
	if (Rx_Buffer_Cnt >= UART_BUF_LEN)
		Rx_Buffer_Cnt = 0x00;

	Rx_TimeOut = FALSE;
}
/*******************************************************************************
 * Function    : Uart_Send_Byte
 * Brief       : 
 * Parameter   : {u8} data-待发送数据
 * Returns     : void
 * Date        : 2021-04-13 17:33:50
 * Description : 串口发送一个字节
 ******************************************************************************/
void Uart_Send_Byte(u8 data)
{
	//usart send data
	USART_SendData8(data);

	/* Wait until end of transmit */
	while (USART_GetFlagStatus(USART_FLAG_TC) == RESET)
	{
		;
	}
}
/*******************************************************************************
 * Function    : Uart_Send_Byte2Ascii
 * Brief       : 
 * Parameter   : {u8} data-待发送数据
 * Returns     : 
 * Date        : 2021-04-13 17:40:28
 * Description : 串口发送一个字节（转换成ASCII）
 ******************************************************************************/
void Uart_Send_Byte2Ascii(u8 data)
{
	Uart_Send_Byte(AHL_Data_Hex2Ascii_4bit(data >> 4));
	Uart_Send_Byte(AHL_Data_Hex2Ascii_4bit(data & 0x0f));
}
/*******************************************************************************
 * Function    : Uart_Send_Array
 * Brief       : 
 * Parameter   : {u8*} data-待发送数组
 * Parameter   : {u8} len-数组长度
 * Returns     : 
 * Date        : 2020-03-24 18:30:47
 * Description : 发送一个数组（原始数据）
 ******************************************************************************/
void Uart_Send_Array(u8 *data, u8 len)
{
	u8 i;
	for (i = 0; i < len; i++)
	{
		Uart_Send_Byte(data[i]);
	}
}
/*******************************************************************************
 * Function    : Uart_Send_Array_R
 * Brief       : 
 * Parameter   : {u8*} data-待发送数组
 * Parameter   : {u8} len-数组长度
 * Returns     : 
 * Date        : 2020-03-24 18:30:47
 * Description : 发送一个数组（原始数据），倒序
 ******************************************************************************/
void Uart_Send_Array_R(u8 *data, u8 len)
{
	u8 i;
	for (i = len; i != 0; i--)
	{
		Uart_Send_Byte(data[i - 1]);
	}
}
/*******************************************************************************
 * Function    : Uart_Send_Array2Ascii
 * Brief       : 
 * Parameter   : {u8*} data-待发送数组
 * Parameter   : {u8} len-数组长度
 * Parameter   : {u8} space-是否空格做间隔
 * Returns     : 
 * Date        : 2020-03-24 18:30:47
 * Description : 发送一个数组（转成ASCII）
 ******************************************************************************/
void Uart_Send_Array2Ascii(u8 *data, u8 len, u8 space)
{
	u8 i;
	for (i = 0; i < len; i++)
	{
		Uart_Send_Byte2Ascii(data[i]);
		if (space)
			Uart_Send_Byte(' '); //插入空格
	}
}
/*******************************************************************************
 * Function    : Uart_Send_Array2Ascii_R
 * Brief       : 
 * Parameter   : {u8*} data-待发送数组
 * Parameter   : {u8} len-待发送数组长度
 * Parameter   : {u8} space-是否空格做间隔
 * Returns     : 
 * Date        : 2020-03-24 18:30:47
 * Description : 发送一个数组（转成ASCII），倒序
 ******************************************************************************/
void Uart_Send_Array2Ascii_R(u8 *data, u8 len, u8 space)
{
	u8 i;
	for (i = len; i != 0; i--)
	{
		Uart_Send_Byte2Ascii(data[i - 1]);
		if (space)
			Uart_Send_Byte(' '); //插入空格
	}
}
/*******************************************************************************
 * Function    : Uart_Send_String
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-13 17:38:41
 * Description : 
 ******************************************************************************/
void Uart_Send_String(u8 *data, u8 len)
{
	unsigned char i = 0;
	//check string end char
	while (i < len)
	{
		//USART_SendData8(*data);
		USART->DR = *data;
		/* Wait until end of transmit */
		while (USART_GetFlagStatus(USART_FLAG_TC) == RESET)
		{
			;
		}
		data++;
		i++;
	}
}
/*******************************************************************************
 * Function    : GetRxTimeout
 * Brief       : 
 * param        {*}
 * return       {*}
 * Description : 
 * Date        : 2021-05-13 10:07:40
 ******************************************************************************/
u8 GetRxTimeout(void)
{
	return Rx_TimeOut;
}
/*******************************************************************************
 * Function    : Uart_Recived
 * Brief       : 
 * Parameter   : {u8 *}data-串口接收到的数据类容
 * Parameter   : {u8 *}len-串口接收到的数据长度
 * Returns     : 
 * Date        : 2021-04-16 10:43:26
 * Description : 串口接收信息处理
 ******************************************************************************/
void Uart_Recived(u8 *data, u8 *len)
{
	static u32 time = 0;

	if ((GetSysTickCount() - time) < 50)
		return;
	time = GetSysTickCount();

	if (Rx_TimeOut == FALSE)
	{ //接收到数据内容，且超时
		Rx_TimeOut = TRUE;
		AHL_Data_Memcpy(data, (u8 *)Rx_Buffer, Rx_Buffer_Cnt);
		*len = Rx_Buffer_Cnt;
		Rx_Buffer_Cnt = 0x00;
	}
	else
		*len = 0;
}