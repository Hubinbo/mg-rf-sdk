/*******************************************************************************
 * FilePath    : /MG127_app_TxRx/main.c
 * Version     : 1.0
 * Brief       : 
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2020-03-24 15:42:28
 * LastEditors : AndrewHu
 * LastEditTime: 2021-05-13 14:57:41
 * Description : 
 ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define NormalMode 0x00 //正常模式(RF Sleep)
#define SleepMode 0x01  //停止工作，进入sleep
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
u8 runningMode = NormalMode; //
u8 UartRxData[50];
u8 UartRxDataLen;

u8 txcnt = 0;
u8 rxcnt = 0;
/* Private function ----------------------------------------------------------*/
/* extern --------------------------------------------------------------------*/
/*******************************************************************************
 * Function    : main
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2020-03-24 16:11:19
 * Description : 启动函数，主循环
 ******************************************************************************/
int main(void)
{
    u32 time;
    u8 bleRunDelay = 0x00;

    Init_System();

    LED_RED_ON();
    Printf_PowerUp(); //上电信息打印

    BLE_Init(BLE_TX_POWER, TRUE); //蓝牙初始化
    Uart_Send_String("\r\nBLE init ok.", 14);

    // Fun_Test_All(); //SRRC/KC认证测试时调用，不返回

    time = GetSysTickCount();
    while (1)
    {
        if ((GetSysTickCount() - time) >= 5)
        { //5ms
            time = GetSysTickCount();
            Key_Scan();
            bleRunDelay++;
            if (bleRunDelay >= 20)
            { //100ms
                bleRunDelay = 0;

                txcnt = 3; //txcnt=0 is for rx only application
                rxcnt = 6; //rxcnt=0 is for tx only application
                BLE_TRX();
            }
        }

        //串口示例，收到数据直接打印
        Uart_Recived(UartRxData, &UartRxDataLen);
        if (UartRxDataLen != 0)
        {
            Uart_Send_String(UartRxData, UartRxDataLen);
            UartRxDataLen = 0;
        }
    }
}
