/*******************************************************************************
 * FilePath    : \00-Inc\AHL_CRC_Includes.h
 * Version     : 1.0
 * Brief       :
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2020-03-24 15:56:39
 * LastEditors : AndrewHu
 * LastEditTime: 2021-05-10 12:31:48
 * Description :
 ******************************************************************************/
#ifndef __AHL_CRC_INCLUDES_H__
#define __AHL_CRC_INCLUDES_H__
/* Includes ------------------------------------------------------------------*/
#include <AH_Lib_Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/*******************************************************************************
 * Function    : AHL_CRC16_IBM
 * Brief       : Modbus CRC16 校验计算函数
 * Parameter   : {u8*} str-待计算的数据首地址
 * Parameter   : {u8} len-数据长度
 * Returns     : {u16} 计算结果
 * Date        : 2020-04-06 15:10:23
 * Description : 
 ******************************************************************************/
u16 AHL_CRC16_IBM(u8 *str, u8 len);
/* extern --------------------------------------------------------------------*/
#endif
