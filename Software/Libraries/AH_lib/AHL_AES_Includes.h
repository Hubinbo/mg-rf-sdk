/*******************************************************************************
 * FilePath    : \00-Inc\AHL_AES_Includes.h
 * Version     : 1.0
 * Brief       :
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2020-03-24 15:56:39
 * LastEditors : AndrewHu
 * LastEditTime: 2021-05-10 12:57:02
 * Description :
 ******************************************************************************/
#ifndef __AHL_AES_INCLUDES_H__
#define __AHL_AES_INCLUDES_H__
/* Includes ------------------------------------------------------------------*/
#include <AH_Lib_Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/* extern --------------------------------------------------------------------*/
#endif
