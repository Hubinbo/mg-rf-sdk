/*******************************************************************************
 * FilePath    : \00-Inc\AHL_Data_Includes.h
 * Version     : 1.0
 * Brief       :
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2020-03-24 15:56:39
 * LastEditors : AndrewHu
 * LastEditTime: 2021-05-10 14:52:45
 * Description :
 ******************************************************************************/
#ifndef __AHL_DATA_INCLUDES_H__
#define __AHL_DATA_INCLUDES_H__
/* Includes ------------------------------------------------------------------*/
#include <AH_Lib_Includes.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/*******************************************************************************
 * Function    : AHL_Data_Arry2String
 * Brief       : 数组转换成字符串
 * Parameter   : {u8*} str-转换结果
 * Parameter   : {u8*} arry-转换前数据
 * Parameter   : {u8*} len-转换前(后)数据长度
 * Returns     :
 * Date        : 2020-04-06 15:10:23
 * Description : 0x15,0x14,0x13->"131415"，注意数组不能溢出
 ******************************************************************************/
void AHL_Data_Arry2String(u8 *str, u8 *arr, u8 *len);
/*******************************************************************************
 * Function    : AHL_Data_Arry2String_R
 * Brief       : 数组转换成字符串(倒序)
 * Parameter   : {u8*} str-转换结果
 * Parameter   : {u8*} arry-转换前数据
 * Parameter   : {u8*} len-转换前(后)数据长度
 * Returns     :
 * Date        : 2020-04-06 15:10:23
 * Description : 0x15,0x14,0x13->"131415"，注意数组不能溢出
 ******************************************************************************/
void AHL_Data_Arry2String_R(u8 *str, u8 *arr, u8 *len);
/*******************************************************************************
 * Function    : AHL_Data_ArryIn
 * Brief       : 将数据写入数组指定位置
 * Parameter   : {u8*} arry-待读写数组
 * Parameter   : {u8} max-数组最大范围
 * Parameter   : {u8*} point-待读写数据的位置指针
 * Parameter   : {u8} data-写入数据
 * Returns     :
 * Date        : 2020-04-06 15:10:23
 * Description : 
 ******************************************************************************/
void AHL_Data_ArryIn(u8 *arry, u8 max, u8 *point, u8 data);
/*******************************************************************************
 * Function    : AHL_Data_ArryOut
 * Brief       : 读出数组指定位置的值
 * Parameter   : {u8*} arry-待读写数组
 * Parameter   : {u8} max-数组最大范围
 * Parameter   : {u8*} point-待读写数据的位置指针
 * Returns     :
 * Date        : 2020-04-06 15:10:23
 * Description : 
 ******************************************************************************/
u8 AHL_Data_ArryOut(u8 *arry, u8 max, u8 *point);
/*******************************************************************************
 * Function    : AHL_Data_Ascii2Hex
 * Brief       : ASCII码转换成16进制数
 * Parameter   : {u8} Ascii-ASCII码
 * Returns     : {u8} -16对应16进制数
 * Date        : 2020-03-24 18:23:15
 * Description : Convert Ascii to Hex,0x32->0x02
 ******************************************************************************/
u8 AHL_Data_Ascii2Hex(u8 Ascii);
/*******************************************************************************
 * Function    : AHL_Data_Ascii2Hex_Double
 * Brief       : 两个ASCII码转成一个16进制数
 * Parameter   : {u8} Ascii1
 * Parameter   : {u8} Ascii2
 * Returns     : {u8} hex
 * Date        : 2020-03-24 18:45:03
 * Description : Convert Ascii to Hex,{'1','2'}->0x12
 ******************************************************************************/
u8 AHL_Data_Ascii2Hex_Double(u8 Ascii1, u8 Ascii2);
/*******************************************************************************
 * Function    : AHL_Data_Hex2Ascii_4bit
 * Brief       : 16进制转换为ASCII
 * Parameter   : {u8} Hex-不大于0x0f
 * Returns     : {u8}
 * Date        : 2020-03-24 18:26:19
 * Description : Convert Hex to Ascii,0x02->0x32('2')
 ******************************************************************************/
u8 AHL_Data_Hex2Ascii_4bit(u8 Hex);
/*******************************************************************************
 * Function    : AHL_Data_Hex2Ascii_8bit
 * Brief       : 16进制转换为ASCII
 * Parameter   : {u8} Hex
 * Returns     : {u16}
 * Date        : 2020-03-24 18:26:19
 * Description : Convert Hex to Ascii,0x32->0x3332('32')
 ******************************************************************************/
u16 AHL_Data_Hex2Ascii_8bit(u8 Hex);
/*******************************************************************************
 * Function    : AHL_Data_Hex2Dec_8bit
 * Brief       : 16进制转换为10进制
 * Parameter   : {u8} hex
 * Returns     : {U16}
 * Date        : 2020-03-25 11:52:40
 * Description : Convert Hex to Dec,0x86(134)->0x0134
 ******************************************************************************/
u16 AHL_Data_Hex2Dec_8bit(u8 hex);
/*******************************************************************************
 * Function    : AHL_Data_Hex2Dec_16bit
 * Brief       : 16进制转换为10进制
 * Parameter   : {u8} hex
 * Returns     : {U32}
 * Date        : 2020-03-25 11:52:40
 * Description : Convert Hex to Dec,0xFFFF->0x00065535
 ******************************************************************************/
u32 AHL_Data_Hex2Dec_16bit(u16 hex);
/*******************************************************************************
 * Function    : AHL_Data_Hex2DecAscii_8bit
 * Brief       : 16进制转换为10进制，且转变为ASCII码
 * Parameter   : {u8} hex-The value to be converted
 * Parameter   : {u8*} string-retrun string index
 * Parameter   : {u8*} len-retrun lenght index
 * Returns     :
 * Date        : 2020-03-25 12:00:13
 * Description : Convert Hex to Dec(Ascii),0x86(134)->{'1','3','4'}
 ******************************************************************************/
void AHL_Data_Hex2DecAscii_8bit(u8 hex, u8 *string, u8 *len);
/*******************************************************************************
 * Function    : AHL_Data_Hex2DecAscii_16bit
 * Brief       : 16进制转换为10进制，且转变为ASCII码
 * Parameter   : {u16} hex-The value to be converted
 * Parameter   : {u8*} string-retrun string index
 * Parameter   : {u8*} len-retrun lenght index
 * Returns     :
 * Date        : 2020-03-25 12:00:13
 * Description : Convert Hex to Dec(Ascii),0xffff(134)->{'6','5','5','3','5'}
 ******************************************************************************/
void AHL_Data_Hex2DecAscii_16bit(u16 hex, u8 *string, u8 *len);
/*******************************************************************************
 * Function    : AHL_Data_String2Arry
 * Brief       : 字符串转换成数组
 * Parameter   : {u8*} arry-转换结果
 * Parameter   : {u8*} str-转换前数据
 * Parameter   : {u8*} len-转换前(后)数据长度
 * Returns     :
 * Date        : 2020-04-06 15:10:23
 * Description : "131415"->0x13,0x14,0x15
 ******************************************************************************/
void AHL_Data_String2Arry(u8 *arr, u8 *str, u8 *len);
/*******************************************************************************
 * Function    : AHL_Data_String2Arry_R
 * Brief       : 字符串转换成数组(倒序)
 * Parameter   : {u8*} arry-转换结果
 * Parameter   : {u8*} str-转换前数据
 * Parameter   : {u8*} len-转换前(后)数据长度
 * Returns     :
 * Date        : 2020-04-06 15:10:23
 * Description : "131415"->0x15,0x14,0x13，注意数组不能溢出
 ******************************************************************************/
void AHL_Data_String2Arry_R(u8 *arr, u8 *str, u8 *len);
/*******************************************************************************
 * Function    : AHL_Data_Compare
 * Brief       : 比较两个字符串是否一致
 * Parameter   : {u8*} arry1-数组1
 * Parameter   : {u8*} arry2-数组2
 * Parameter   : {u8} len-长度
 * Returns     : {u8} 0-不一致，1-一致
 * Date        : 2020-04-06 15:10:23
 * Description : 
 ******************************************************************************/
u8 AHL_Data_Compare(u8 *arry1, u8 *arry2, u8 len);
/*******************************************************************************
 * Function    : AHL_Data_Memcpy
 * Brief       : 数据搬移
 * Parameter   : {u8*} tgtArry-目标数组
 * Parameter   : {u8*} objArry-待搬移数据
 * Parameter   : {u8} len-待搬移数据长度
 * Returns     :
 * Date        : 2021-04-06 15:10:23
 * Description : 注意数组不能溢出
 ******************************************************************************/
void AHL_Data_Memcpy(u8 *tgtArry, u8 *objArry, u8 len);
/*******************************************************************************
 * Function    : AHL_Data_BlankCheck
 * Brief       : 数据查空
 * Parameter   : {u8*} arry-待检查数据
 * Parameter   : {u8} len-数据长度
 * Returns     : {u8} 1-数据为空，0-数据不为空
 * Date        : 2021-04-16 15:10:23
 * Description : 
 ******************************************************************************/
u8 AHL_Data_BlankCheck(u8 *arr, u8 len);
/* extern --------------------------------------------------------------------*/
#endif
