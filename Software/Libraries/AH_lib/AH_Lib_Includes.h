/*******************************************************************************
 * FilePath    : /Software/Libraries/AH_lib/AH_Lib_Includes.h
 * Version     : 1.0
 * Brief       :
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2020-03-24 15:56:39
 * LastEditors : AndrewHu
 * LastEditTime: 2021-05-13 10:24:35
 * Description :
 ******************************************************************************/
#ifndef __AH_LIB_INCLUDES_H__
#define __AH_LIB_INCLUDES_H__
/* Includes ------------------------------------------------------------------*/
#ifndef sysIncludes
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#endif
/* Private typedef -----------------------------------------------------------*/
#ifndef dataTypeDef
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
#endif
/* Private define ------------------------------------------------------------*/
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif
/* Private macro -------------------------------------------------------------*/
#ifndef assert_param
#ifdef USE_FULL_ASSERT
/*******************************************************************************
 * Function    : assert_param
 * Brief       : The assert_param macro is used for function's parameters check.
 * Parameter   : expr-If expr is false, it calls assert_failed function
 *               which reports the name of the source file and the source
 *               line number of the call that failed.
 *               If expr is true, it returns no value.
 * Returns     : None
 * Date        : 2020-05-11 15:22:18
 * Description :
 ******************************************************************************/
#define assert_param(expr) \
  \((expr) ? (void)0 : assert_failed((uint8_t *)__FILE__, __LINE__))
void assert_failed(uint8_t *file, uint32_t line);
#else
#define assert_param(expr) ((void)0)
#endif /* USE_FULL_ASSERT */
#endif /* assert_param */
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/* extern --------------------------------------------------------------------*/
#include <AHL_AES_Includes.h>
#include <AHL_CRC_Includes.h>
#include <AHL_Data_Includes.h>
#endif
