/*******************************************************************************
 * FilePath    : \Software\Examples_MG223\MG223_app_RfTest\Src\User\User_Printf.c
 * Version     : 1.0
 * Brief       : 
 * Author      : AndrewHu
 * Company     : Shanghai MacroGiga Electronics CO.,Ltd
 * Email       : Hubinbo@macrogiga.com
 * Date        : 2021-04-13 17:13:21
 * LastEditors : AndrewHu
 * LastEditTime: 2021-04-16 19:15:04
 * Description : 
 ******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "Includes.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function ----------------------------------------------------------*/
/* extern --------------------------------------------------------------------*/
/*******************************************************************************
 * Function    : Printf_OK
 * Brief       : 
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 17:19:07
 * Description : 
 ******************************************************************************/
void Printf_OK(void)
{
	Uart_Send_String("\r\nIND:OK", 8);
}
/*******************************************************************************
 * Function    : Printf_Error
 * Brief       : 发送错误及错误代码
 * Parameter   : 
 * Returns     : 
 * Date        : 2021-04-16 17:19:07
 * Description : 
 ******************************************************************************/
void Printf_Error(u8 ErrorNum)
{
	Uart_Send_String("\r\nIND:Error(", 12);
	Uart_Send_Byte2Ascii(ErrorNum);
	Uart_Send_Byte((u8)')');
}
/*******************************************************************************
 * Function    : Printf_NewLine
 * Brief       : 
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-13 17:33:25
 * Description : 打印回车换行
 ******************************************************************************/
void Printf_NewLine(void)
{
	Uart_Send_String("\r\n", 2);
}
/*******************************************************************************
 * Function    : Printf_ListSeparator_8byte
 * Brief       : 
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-14 11:30:47
 * Description : 分隔符
 ******************************************************************************/
void Printf_ListSeparator_8byte(void)
{

	Uart_Send_String("----", 8);
}
/*******************************************************************************
 * Function    : Printf_ListSpace_8byte
 * Brief       : 
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-14 11:30:47
 * Description : 空格
 ******************************************************************************/
void Printf_ListSpace_8byte(void)
{

	Uart_Send_String("    ", 4);
}
/*******************************************************************************
 * Function    : Printf_PowerUp
 * Brief       : 
 * Parameter   : void
 * Returns     : void
 * Date        : 2021-04-14 11:08:08
 * Description : 上电打印
 ******************************************************************************/
void Printf_PowerUp(void)
{
	Uart_Send_String((u8 *)"\r\n----------------------------------------", 42);
	Uart_Send_String((u8 *)"\r\n-Shanghai MacroGiga Electronics CO.,Ltd-", 42);
	Uart_Send_String((u8 *)"\r\n---------Software Version: V2.2---------", 42);

	Uart_Send_String((u8 *)"\r\n----------", 12);
	Uart_Send_String(__DATE__, sizeof(__DATE__) - 1);
	Uart_Send_Byte((u8)' ');
	Uart_Send_String(__TIME__, sizeof(__TIME__) - 1);
	Uart_Send_String((u8 *)"----------", 10);

	Uart_Send_String((u8 *)"\r\n----------------------------------------", 42);
}
